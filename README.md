<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/SamirPomak/xplored-social-network">
    <img src="https://i.imgur.com/0EKuJS7.png" alt="Logo" width="150" height="150">
  </a>

<h1 align="center">xplored</h1>

  <p align="center">
    Social network for travel lovers.
    <br />
    <br />
    <a href="https://gitlab.com/SamirPomak/xplored-social-network/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/SamirPomak/xplored-social-network/-/issues">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#getting-started">Prerequisites</a></li>
        <li><a href="#getting-started">Installation</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Usage</a>
      <ul>
        <li><a href="#desktop-view">Desktop View</a></li>
        <li><a href="#mobile-view">Mobile View</a></li>
      </ul>
    </li>
    <li><a href="#authors">Authors</a></li>
  </ol>
</details>
<div id="about-the-project"></div>
<!-- ABOUT THE PROJECT -->

## 📋 About The Project

<div align="center">
<img src="https://i.imgur.com/rt72Eii.png" alt="Logo" width="600" height="400">
</div>

Social network application where you can make connections with other users, post content and comment on content of the platform’s users.

<p align="right">(<a href="#top">back to top</a>)</p>
<div id="built-with"></div>

<!-- Bult with -->

### 🛠 Built With

- [React.js](https://reactjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [Material UI](https://mui.com/)

<p align="right">(<a href="#top">back to top</a>)</p>
<div id="getting-started"></div>
<!-- GETTING STARTED -->

## 🚀 Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

- Node.js
- npm

### Installation

- Clone the repo
  ```sh
  git clone https://gitlab.com/SamirPomak/xplored-social-network.git
  ```
  Install NPM packages for both the client and the server
- client
  ```sh
  cd client/
  npm install
  ```
- server
  ```sh
  cd server/
  npm install
  ```

<p align="right">(<a href="#top">back to top</a>)</p>
<div id="usage"></div>
<!-- USAGE EXAMPLES -->

## 💻Usage

- **Start the server**

```sh
 cd server/
 npm start
```

- **Start the client**

```sh
cd client/
npm start
```

<br/>
<h5 align="center">Desktop view</h5>
<div id="desktop-view" align="center">
<img src="https://i.imgur.com/cG5MgEY.png" alt="Logo" width="600" height="400">
</div>
<br/>
<h5 align="center">Mobile view</h5>
<div id="mobile-view" align="center">
<img src="https://i.imgur.com/OP7z2Zu.png" alt="Logo" width="600" height="400">
</div>
<p align="right">(<a href="#top">back to top</a>)</p>

<div id="authors"></div>
<!-- CONTACT -->

## 🧭 Authors

👨‍💻 **Samir Pomak**: [<img src="https://cdn-icons-png.flaticon.com/512/174/174857.png" alt="Logo" width="25" height="25">](https://www.linkedin.com/in/samir-pomak-a93841204/)
<br/>
<br/>
👨‍💻 **Ivan Stamboliyski**: [<img src="https://cdn-icons-png.flaticon.com/512/174/174857.png" alt="Logo" width="25" height="25">](https://www.linkedin.com/in/ivan-stamboliyski-ba72b7226/)

<p align="right">(<a href="#top">back to top</a>)</p>

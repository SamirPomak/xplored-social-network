import './App.css';
import CssBaseline from '@mui/material/CssBaseline';
import PublicHeader from './components/Header/PublicHeader';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import { routes } from './common/constants';
import PublicView from './views/PublicView/PublicView';
import AboutView from './views/AboutView/AboutView';
import PrivateHeader from './components/Header/PrivateHeader';
import SignUp from './views/SignUp/SignUp';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from './theme/theme';
import AuthContext from './context/AuthContext';
import { useContext, useEffect, useState } from 'react';
import { getUser } from './common/helpers';
import { IAuthState, IUserDetails } from './common/interfaces';
import CustomSnackbar from './components/Snackbar/CustomSnackbar';
import Users from './components/Users/Users';
import OwnUserProfile from './components/Users/OwnUserProfile/OwnUserProfile';
import SignIn from './views/SignIn/SignIn';
import { getUserDetails } from './services/requests';
import SnackContext from './context/SnackContext';
import AppContext from './context/AppContext';
import UserProfile from './components/Users/UserProfile/UserProfile';
import PrivateHomeView from './views/PrivateHomeView/PrivateHomeView';
import AdminTable from './components/AdminTable/AdminTable';
import BannedView from './views/BannedView/BannedView';
import PageNotFound from './views/PageNotFoundView/PageNotFound';
import RequireAuth from './components/RequireAuth/RequireAuth';

function App() {
  const [{ isLoggedIn, user }, setAuth] = useState<IAuthState>({
    isLoggedIn: !!getUser(),
    user: getUser(),
  });
  const [userDetails, setUserDetails] = useState<IUserDetails>({
    avatar: 'default.png',
  } as IUserDetails);
  const [trigger, setTrigger] = useState(false);
  const [searchKeyword, setSearchKeyword] = useState('');
  const { setSnack } = useContext(SnackContext);

  useEffect(() => {
    if (isLoggedIn) {
      getUserDetails(user?.id, setUserDetails, setSnack);
    }
  }, [user, trigger]);

  useEffect(() => {
    setSearchKeyword('');
  }, [isLoggedIn]);

  if (userDetails.banDate) {
    return (
      <BannedView
        date={userDetails.banDate as string}
        reason={userDetails.banReason as string}
      />
    );
  }

  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <AuthContext.Provider value={{ isLoggedIn, user, setAuth }}>
          <AppContext.Provider
            value={{
              userDetails,
              setUserDetails,
              setTrigger,
              searchKeyword,
              setSearchKeyword,
            }}
          >
            <CustomSnackbar>
              <div className="App">
                <CssBaseline />
                {isLoggedIn ? <PrivateHeader /> : <PublicHeader />}
                <Routes>
                  <Route path="/" element={<Navigate to={routes.home} />} />
                  <Route
                    path={routes.home}
                    element={isLoggedIn ? <PrivateHomeView /> : <PublicView />}
                  />
                  <Route path={routes.about} element={<AboutView />} />
                  <Route path={routes.signin} element={<SignIn />} />
                  <Route path={routes.signup} element={<SignUp />} />
                  <Route
                    path={routes.admin}
                    element={
                      <RequireAuth redirectTo={routes.signin}>
                        {user?.role === 2 ? <AdminTable /> : <PageNotFound />}
                      </RequireAuth>
                    }
                  />
                  <Route path={routes.users} element={<Users />}>
                    <Route path="me" element={<OwnUserProfile />} />
                    <Route
                      path={`${user?.id}`}
                      element={
                        <RequireAuth redirectTo={routes.signin}>
                          <OwnUserProfile />
                        </RequireAuth>
                      }
                    />
                    <Route
                      path=":id"
                      element={
                        <RequireAuth redirectTo={routes.signin}>
                          <UserProfile />
                        </RequireAuth>
                      }
                    />
                  </Route>
                  <Route path="*" element={<PageNotFound />} />
                </Routes>
              </div>
            </CustomSnackbar>
          </AppContext.Provider>
        </AuthContext.Provider>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;

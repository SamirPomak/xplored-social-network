import { styled } from '@mui/system';
import { SxProps } from '@mui/system';

const Input = styled('input')({
  display: 'none',
});

const UploadAvatar: SxProps = {
  position: 'absolute',
  top: 155,
  right: 0,
  bgcolor: 'gainsboro',
};

const ModalStyle: SxProps = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  maxWidth: 600,
  maxHeight: 600,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  overflow: 'scroll',
};

export { Input, UploadAvatar, ModalStyle };

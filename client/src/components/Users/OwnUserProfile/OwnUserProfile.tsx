import Avatar from '@mui/material/Avatar';
import {
  Stack,
  Typography,
  Box,
  Button,
  Modal,
  TextField,
  InputLabel,
} from '@mui/material';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import EditIcon from '@mui/icons-material/Edit';
import IconButton from '@mui/material/IconButton';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import { ModalStyle, UploadAvatar } from './styled';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { SyntheticEvent, useContext, useState } from 'react';
import {
  validateCredentialLength,
  validateEmail,
} from '../../../common/helpers';
import AppContext from '../../../context/AppContext';
import { updateUserDetails } from '../../../services/requests';
import SnackContext from '../../../context/SnackContext';
import AuthContext from '../../../context/AuthContext';
import Connections from '../../Connections/Connections';
import UserPosts from '../../../views/UserPosts/UserPosts';

const OwnUserProfile = () => {
  const { userDetails, setUserDetails } = useContext(AppContext);
  const { user, setAuth } = useContext(AuthContext);
  const { setSnack } = useContext(SnackContext);
  const [tabValue, setTabValue] = useState(1);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [editModalForm, setEditModalForm] = useState({
    password: '',
    newPassword: '',
    email: user?.email || '',
  });

  const handleTabChange = (e: SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  const handleSaveEdit = (e: SyntheticEvent) => {
    e.preventDefault();
    const form = new FormData(e.currentTarget as HTMLFormElement);

    Object.entries(editModalForm).forEach(([key, value]) => {
      if (!value) {
        form.delete(key);
      }
    });

    if (form.has('email')) {
      updateUserDetails(form, setUserDetails, setSnack, setAuth);
    } else {
      updateUserDetails(form, setUserDetails, setSnack);
    }

    setIsEditModalOpen(false);
  };

  const tabPanels = [
    <UserPosts key="userposts" id={user?.id as number} />,
    <Connections key="connections" details={userDetails} />,
  ];

  return (
    <>
      <Stack justifyContent="center" alignItems="center" spacing={3}>
        <Box sx={{ alignSelf: 'flex-end' }}>
          <Button
            onClick={() => setIsEditModalOpen(true)}
            variant="outlined"
            startIcon={<EditIcon />}
          >
            Edit Profile
          </Button>
        </Box>
        <div style={{ position: 'relative' }}>
          <Avatar
            alt="profile picture"
            src={`http://localhost:5000/images/${userDetails.avatar}`}
            sx={{
              width: '12.2rem',
              height: '12.2rem',
            }}
          />
          <form>
            <label htmlFor="icon-button-file">
              <IconButton
                onClick={() => setIsEditModalOpen(true)}
                color="primary"
                aria-label="upload picture"
                component="span"
                sx={UploadAvatar}
              >
                <PhotoCamera fontSize="large" />
              </IconButton>
            </label>
          </form>
        </div>

        <Typography variant="h4">{userDetails.username}</Typography>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs value={tabValue} onChange={handleTabChange}>
            <Tab label="Posts" value={1} />
            <Tab label="Connections" value={2} />
          </Tabs>
        </Box>
        {tabPanels[tabValue - 1]}
      </Stack>
      <Modal
        open={isEditModalOpen}
        id="edit-modal"
        onClose={() => setIsEditModalOpen(false)}
        aria-labelledby="edit-modal-title"
        aria-describedby="edit-modal-description"
      >
        <Box sx={ModalStyle}>
          <Typography variant="h6">Edit user profile</Typography>
          <hr />
          <Box component="form" onSubmit={handleSaveEdit}>
            <TextField
              error={
                !validateCredentialLength(editModalForm.password, 'password')
              }
              helperText={
                !validateCredentialLength(editModalForm.password, 'password') &&
                'Password must be between 6-40 characters'
              }
              type={showPassword ? 'text' : 'password'}
              id="password"
              name="password"
              label="Password"
              value={editModalForm.password}
              onChange={(e) =>
                setEditModalForm((prevState) => ({
                  ...prevState,
                  password: e.target.value,
                }))
              }
              fullWidth
              sx={{ pb: 2 }}
            />

            <TextField
              error={
                !validateCredentialLength(editModalForm.newPassword, 'password')
              }
              helperText={
                !validateCredentialLength(
                  editModalForm.newPassword,
                  'password'
                ) && 'Password must be between 6-40 characters'
              }
              type={showPassword ? 'text' : 'password'}
              id="newPassword"
              name="newPassword"
              label="New Password"
              value={editModalForm.newPassword}
              onChange={(e) =>
                setEditModalForm((prevState) => ({
                  ...prevState,
                  newPassword: e.target.value,
                }))
              }
              fullWidth
              sx={{ pb: 2 }}
            />

            <TextField
              error={!validateEmail(editModalForm.email)}
              helperText={
                !validateEmail(editModalForm.email) && 'Invalid email address!'
              }
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              fullWidth
              value={editModalForm.email}
              onChange={(e) =>
                setEditModalForm((prevState) => ({
                  ...prevState,
                  email: e.target.value,
                }))
              }
              sx={{ pb: 2 }}
            />

            <InputLabel
              shrink
              htmlFor="avatar"
              sx={{ fontSize: '1.3em', fontWeight: 500 }}
            >
              Upload avatar
            </InputLabel>
            <TextField
              fullWidth
              name="file"
              type="file"
              id="avatar"
              autoComplete="new-avatar"
              inputProps={{ accept: 'image/*' }}
              sx={{ pb: 2 }}
            />
            <FormControlLabel
              onChange={() => setShowPassword(!showPassword)}
              sx={{ float: 'right', mt: -1 }}
              control={<Checkbox value="show-pass" color="primary" />}
              label="Show passwords"
            />
            <br />
            <Button type="submit" size="small" color="error">
              Save
            </Button>
            <Button onClick={() => setIsEditModalOpen(false)} size="small">
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default OwnUserProfile;

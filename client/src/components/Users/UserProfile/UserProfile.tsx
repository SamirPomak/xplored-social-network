import { SyntheticEvent, useContext, useEffect, useState } from 'react';
import Avatar from '@mui/material/Avatar';
import { Stack, Typography, Box } from '@mui/material';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useParams } from 'react-router';
import { IUserDetails } from '../../../common/interfaces';
import { getUserDetails } from '../../../services/requests';
import SnackContext from '../../../context/SnackContext';
import ConnectButton from '../../ConnectButton/ConnectButton';
import Connections from '../../Connections/Connections';
import UserPosts from '../../../views/UserPosts/UserPosts';

const UserProfile = () => {
  const [tabValue, setTabValue] = useState(1);
  const [profileDetails, setProfileDetails] = useState<IUserDetails>({
    avatar: 'default.png',
  } as IUserDetails);
  const { setSnack } = useContext(SnackContext);
  const [isFound, setIsFound] = useState(true);
  const params = useParams() as { id: string };

  useEffect(() => {
    getUserDetails(+params.id, setProfileDetails, setSnack).catch(() =>
      setIsFound(false)
    );
  }, [params.id]);

  const handleTabChange = (e: SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  if (!isFound) {
    return <h1>User not found!</h1>;
  }

  const tabPanels = [
    <UserPosts key="userposts" id={+params.id} />,
    <Connections key="connections" details={profileDetails} />,
  ];

  return (
    <>
      <Stack justifyContent="center" alignItems="center" spacing={3}>
        <div style={{ position: 'relative' }}>
          <Avatar
            alt="profile picture"
            src={`http://localhost:5000/images/${profileDetails.avatar}`}
            sx={{
              width: '12.2rem',
              height: '12.2rem',
            }}
          />
        </div>

        <Typography variant="h4">{profileDetails.username}</Typography>
        <ConnectButton size="large" id={+params.id} />
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs value={tabValue} onChange={handleTabChange}>
            <Tab label="Posts" value={1} />
            <Tab label="Connections" value={2} />
          </Tabs>
        </Box>
        {tabPanels[tabValue - 1]}
      </Stack>
    </>
  );
};

export default UserProfile;

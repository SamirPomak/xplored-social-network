import { Container } from '@mui/material';
import { Outlet, useLocation } from 'react-router-dom';
import AllPublicUser from '../../views/AllPublicUser/AllPublicUser';

const Users = () => {
  const location = useLocation();

  return (
    <Container sx={{ mt: 12, justifyContent: 'center', textAlign: 'center' }}>
      {location.pathname === '/users' && <AllPublicUser />}
      <Outlet />
    </Container>
  );
};

export default Users;

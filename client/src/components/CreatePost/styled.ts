import { styled } from '@mui/system';
import { SxProps } from '@mui/system';

export const CreatePostButton = styled('button')(({ theme }) => ({
  borderRadius: '35px',
  fontSize: '1.6rem',
  border: '1px solid rgba(0, 0, 0, 0.3)',
  lineHeight: 1.5,
  minWidth: '480px',
  textAlign: 'left',
  [theme.breakpoints.down('sm')]: {
    minWidth: '250px',
  },
  backgroundColor: 'white',
  '&:hover': {
    cursor: 'pointer',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
}));

export const CreatePostContainerStyle: SxProps = {
  display: 'flex',
  pl: 1,
  pr: 2,
  mt: 10,
  border: '1px solid whitesmoke',
  borderRadius: '0.8rem',
  height: '5rem',
  alignItems: 'center',
  backgroundColor: 'white',
};

export const CreateCommentContainerStyle: SxProps = {
  display: 'flex',
  pl: 1,
  pr: 2,
  mt: 1,
  height: '5rem',
  alignItems: 'center',
  backgroundColor: 'white',
};

import { Box } from '@mui/system';
import Avatar from '@mui/material/Avatar';
import {
  Dispatch,
  SetStateAction,
  SyntheticEvent,
  useContext,
  useState,
} from 'react';
import AppContext from '../../context/AppContext';
import {
  CreateCommentContainerStyle,
  CreatePostButton,
  CreatePostContainerStyle,
} from './styled';
import { ModalStyle } from '../Users/OwnUserProfile/styled';
import {
  Typography,
  Button,
  Modal,
  TextField,
  InputLabel,
  FormControl,
  Stack,
  NativeSelect,
} from '@mui/material';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import ReactPlayer from 'react-player';
import SnackContext from '../../context/SnackContext';
import { createComment, createPost } from '../../services/requests';
import { IPost } from '../../common/interfaces';
import { postTypes } from '../../common/constants';

const CreatePost = ({
  setPosts,
  postType,
  postId,
}: {
  setPosts: Dispatch<SetStateAction<IPost[]>>;
  postType: postTypes;
  postId?: number;
}) => {
  const { userDetails } = useContext(AppContext);
  const { setSnack } = useContext(SnackContext);
  const [isCreatePostModalOpen, setIsCreatePostModalOpen] = useState(false);
  const [postModalForm, setPostModalForm] = useState({
    embed: '',
    media: 'picture',
  });

  const handleMediaSelect = (event: SelectChangeEvent) => {
    setPostModalForm({
      embed: '',
      media: event.target.value as string,
    });
  };

  const handlePost = (e: SyntheticEvent) => {
    e.preventDefault();
    const form = new FormData(e.currentTarget as HTMLFormElement);

    if (postType === postTypes.post) {
      createPost(form, setPosts, setSnack);
    } else {
      createComment(form, postId as number, setPosts, setSnack);
    }

    setPostModalForm({ embed: '', media: 'picture' });
    setIsCreatePostModalOpen(false);
  };

  const closePostEditModal = () => {
    setPostModalForm({ media: 'picture', embed: '' });
    setIsCreatePostModalOpen(false);
  };

  return (
    <>
      <Box
        sx={
          postType === postTypes.post
            ? CreatePostContainerStyle
            : CreateCommentContainerStyle
        }
      >
        <Avatar
          alt={userDetails.username}
          src={`http://localhost:5000/images/${
            userDetails.avatar ?? 'default.png'
          }`}
          sx={{ width: 56, height: 56, mr: 2 }}
        />
        <CreatePostButton onClick={() => setIsCreatePostModalOpen(true)}>
          {postType === postTypes.post ? 'Create a post' : 'Add a comment'}
        </CreatePostButton>
      </Box>
      <Modal
        open={isCreatePostModalOpen}
        id="edit-modal"
        onClose={closePostEditModal}
        aria-labelledby="edit-modal-title"
        aria-describedby="edit-modal-description"
      >
        <Box sx={ModalStyle}>
          <Typography variant="h6">
            {postType === postTypes.post ? 'Create a post' : 'Add a comment'}
          </Typography>
          <hr />
          <Box component="form" onSubmit={handlePost}>
            <Stack direction="row" spacing={2}>
              <Avatar
                alt={userDetails.username}
                src={`http://localhost:5000/images/${
                  userDetails.avatar ?? 'default.png'
                }`}
                sx={{ width: 80, height: 80, mr: 1 }}
              />
              <Stack>
                <Typography variant="h6" sx={{ pl: 1 }}>
                  {userDetails.username}
                </Typography>
                {postType === postTypes.post && (
                  <FormControl sx={{ m: 1, minWidth: 80 }}>
                    <InputLabel
                      variant="standard"
                      htmlFor="uncontrolled-native"
                    >
                      Visibility
                    </InputLabel>
                    <NativeSelect
                      defaultValue="true"
                      inputProps={{
                        name: 'isPublic',
                        id: 'uncontrolled-native',
                      }}
                    >
                      <option value="true">Public</option>
                      <option value="false">Connections only</option>
                    </NativeSelect>
                  </FormControl>
                )}
              </Stack>
            </Stack>
            <TextField
              type="text"
              name="content"
              id="description"
              label="Description"
              minRows={4}
              fullWidth
              multiline
              margin="normal"
              sx={{ pb: 2 }}
            />
            <FormControl fullWidth sx={{ pb: 2 }}>
              <InputLabel id="media-label">Media</InputLabel>
              <Select
                labelId="media-label"
                id="media-select"
                value={postModalForm.media}
                label="Media"
                onChange={handleMediaSelect}
              >
                <MenuItem value="picture">Upload a picture</MenuItem>
                <MenuItem value="video">Link a video</MenuItem>
              </Select>
            </FormControl>

            {postModalForm.media === 'video' && (
              <TextField
                type="text"
                helperText="Accepts all video formats/Youtube/Vimeo/Twitch"
                name="embed"
                id="link"
                label="Video Link"
                fullWidth
                margin="normal"
                value={postModalForm.embed}
                onChange={(e) =>
                  setPostModalForm((prevState) => ({
                    ...prevState,
                    embed: e.target.value,
                  }))
                }
                sx={{ pb: 2 }}
              />
            )}

            {postModalForm.media === 'picture' && (
              <>
                <InputLabel
                  shrink
                  htmlFor="avatar"
                  sx={{ fontSize: '1.3em', fontWeight: 500 }}
                >
                  Upload picture
                </InputLabel>
                <TextField
                  fullWidth
                  name="file"
                  type="file"
                  id="avatar"
                  autoComplete="new-avatar"
                  inputProps={{ accept: 'image/*' }}
                  sx={{ pb: 2, width: ['auto', 'auto', '540px'] }}
                />
              </>
            )}
            <br />
            {postModalForm.embed.length > 0 && (
              <Box
                sx={{
                  height: ['auto', 'auto', '300px'],
                  width: ['auto', 'auto', '500px'],
                }}
              >
                <ReactPlayer
                  width="100%"
                  height="100%"
                  url={postModalForm.embed}
                  controls
                />
              </Box>
            )}
            <br />
            <Button type="submit" size="large" color="primary">
              Post
            </Button>
            <Button onClick={closePostEditModal} size="large" color="error">
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default CreatePost;

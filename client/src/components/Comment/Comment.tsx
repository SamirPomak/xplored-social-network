import {
  Avatar,
  Stack,
  Typography,
  IconButton,
  Button,
  Modal,
  TextField,
  InputLabel,
  FormControl,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from '@mui/material';
import { Box } from '@mui/system';
import { IComment, IPost } from '../../common/interfaces';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import {
  Dispatch,
  SetStateAction,
  SyntheticEvent,
  useContext,
  useState,
} from 'react';
import { ModalStyle } from '../Users/OwnUserProfile/styled';
import AuthContext from '../../context/AuthContext';
import SnackContext from '../../context/SnackContext';
import ReactPlayer from 'react-player';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import {
  avatarStyles,
  commentBoxStyles,
  commentContainerStyles,
  mediaContainer,
} from './styled';
import { userRoles, reactionType } from '../../common/constants';
import {
  deleteComment,
  editComment,
  reactToComment,
} from '../../services/requests';
import LikesContainer from '../LikesContainer/LikesContainer';

const Comment = ({
  comment,
  postId,
  setPosts,
}: {
  comment: IComment;
  postId: number;
  setPosts: Dispatch<SetStateAction<IPost[]>>;
}) => {
  const { user } = useContext(AuthContext);
  const { setSnack } = useContext(SnackContext);
  const [isLiked, setIsLiked] = useState(
    !!comment.likes.find((like) => like.id === user?.id && like.reaction === 1)
  );
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [modalForm, setModalForm] = useState({
    embed: comment.embed || '',
    media: comment.embed ? 'video' : 'picture',
  });

  const handleLikeClick = () => {
    if (isLiked) {
      reactToComment(
        comment.id,
        user,
        postId,
        reactionType.dislike,
        setPosts,
        setSnack
      );
    } else {
      reactToComment(
        comment.id,
        user,
        postId,
        reactionType.like,
        setPosts,
        setSnack
      );
    }

    setIsLiked((likeStatus) => !likeStatus);
  };

  const handleDelete = () => {
    deleteComment(comment.id, postId, setPosts, setSnack);
  };

  const handleMediaSelect = (event: SelectChangeEvent) => {
    setModalForm((prevState) => ({
      ...prevState,
      media: event.target.value as string,
    }));
  };

  const handlePost = (e: SyntheticEvent) => {
    e.preventDefault();
    const form = new FormData(e.currentTarget as HTMLFormElement);

    editComment(comment.id, postId, form, setPosts, setSnack);
    setIsModalOpen(false);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Box sx={commentBoxStyles}>
        <Stack direction="row" spacing={1}>
          <Avatar
            alt={comment.author.username}
            src={`http://localhost:5000/images/${comment.author.avatar ?? 'default.png'
              }`}
            sx={avatarStyles}
          />
          <Stack spacing={2} sx={commentContainerStyles}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <Stack>
                <Typography
                  variant="h6"
                  sx={{ fontSize: ['0.9rem', '1.1rem'] }}
                >
                  {comment.author.username}
                </Typography>
                <Typography
                  variant="subtitle1"
                  sx={{ fontSize: ['0.6rem', '0.9rem'] }}
                >
                  {new Date(comment.createdOn).toLocaleString()}
                </Typography>
              </Stack>
              <Box>
                {comment.author.id === user?.id && (
                  <IconButton
                    onClick={() => setIsModalOpen(true)}
                    color="primary"
                  >
                    <EditIcon />
                  </IconButton>
                )}
                {(comment.author.id === user?.id ||
                  user?.role === userRoles.admin) && (
                    <IconButton
                      color="error"
                      onClick={() => setOpenDeleteDialog(true)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  )}
                <IconButton onClick={handleLikeClick}>
                  {isLiked ? (
                    <FavoriteIcon fontSize="medium" color="error" />
                  ) : (
                    <FavoriteBorderIcon fontSize="medium" />
                  )}
                </IconButton>
              </Box>
            </Box>
            {comment.content && (
              <Typography sx={{ ml: 2, mt: 3 }}>{comment.content}</Typography>
            )}
            {(comment.embed || comment.picture) && (
              <Box sx={mediaContainer}>
                {comment.picture ? (
                  <img
                    src={`http://localhost:5000/images/${comment.picture}`}
                    alt={comment.picture}
                    style={{
                      height: '300px',
                      width: '100%',
                      objectFit: 'scale-down',
                    }}
                  />
                ) : (
                  <ReactPlayer
                    width="100%"
                    height="300px"
                    url={comment.embed as string}
                    controls
                  />
                )}
              </Box>
            )}
            <LikesContainer users={comment.likes} />
          </Stack>
        </Stack>
      </Box>

      <Modal
        open={isModalOpen}
        id="edit-modal"
        onClose={closeModal}
        aria-labelledby="edit-modal-title"
        aria-describedby="edit-modal-description"
      >
        <Box sx={ModalStyle}>
          <Typography variant="h6">Edit Comment</Typography>
          <hr />
          <Box component="form" onSubmit={handlePost}>
            <Stack direction="row" spacing={2}>
              <Avatar
                alt={comment.author.username}
                src={`http://localhost:5000/images/${comment.author.avatar ?? 'default.png'
                  }`}
                sx={avatarStyles}
              />
              <Stack>
                <Typography variant="h6" sx={{ pl: 1 }}>
                  {comment.author.username}
                </Typography>
              </Stack>
            </Stack>
            <TextField
              defaultValue={comment.content ?? ''}
              type="text"
              name="content"
              id="description"
              label="Description"
              minRows={4}
              fullWidth
              multiline
              margin="normal"
              sx={{ pb: 2 }}
            />
            <FormControl fullWidth sx={{ pb: 2 }}>
              <InputLabel id="media-label">Media</InputLabel>
              <Select
                labelId="media-label"
                id="media-select"
                value={modalForm.media}
                label="Media"
                onChange={handleMediaSelect}
              >
                <MenuItem value="picture">Upload a picture</MenuItem>
                <MenuItem value="video">Link a video</MenuItem>
              </Select>
            </FormControl>

            {modalForm.media === 'video' && (
              <TextField
                type="text"
                helperText="Accepts all video formats/Youtube/Vimeo/Twitch"
                name="embed"
                id="link"
                label="Video Link"
                fullWidth
                margin="normal"
                value={modalForm.embed}
                onChange={(e) =>
                  setModalForm((prevState) => ({
                    ...prevState,
                    embed: e.target.value,
                  }))
                }
                sx={{ pb: 2 }}
              />
            )}

            {modalForm.media === 'picture' && (
              <>
                <InputLabel
                  shrink
                  htmlFor="avatar"
                  sx={{ fontSize: '1.3em', fontWeight: 500 }}
                >
                  Upload picture
                </InputLabel>
                <TextField
                  fullWidth
                  name="file"
                  type="file"
                  id="avatar"
                  autoComplete="new-avatar"
                  inputProps={{ accept: 'image/*' }}
                  sx={{ pb: 2, width: ['auto', 'auto', '540px'] }}
                />
              </>
            )}
            <br />
            {modalForm.embed.length > 0 && (
              <Box
                sx={{
                  height: ['auto', 'auto', '300px'],
                  width: ['auto', 'auto', '500px'],
                }}
              >
                <ReactPlayer
                  width="100%"
                  height="100%"
                  url={modalForm.embed}
                  controls
                />
              </Box>
            )}
            <br />
            <Button type="submit" size="large" color="primary">
              Post
            </Button>
            <Button onClick={closeModal} size="large" color="error">
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>

      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Delete this comment?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDelete}>Yes</Button>
          <Button onClick={() => setOpenDeleteDialog(false)} autoFocus>
            No
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Comment;

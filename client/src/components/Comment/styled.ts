import { SxProps } from '@mui/system';

export const commentContainerStyles: SxProps = {
  pl: 1,
  pt: 1,
  backgroundColor: 'rgba(0, 0, 0, 0.1)',
  border: '1px solid whitesmoke',
  borderRadius: '0.8rem',
  width: '100%',
};

export const avatarStyles: SxProps = { width: [40, 60], height: [40, 60] };

export const mediaContainer: SxProps = {
  mt: 3,
  mb: 3,
  maxHeight: '300px',
  width: 'auto',
  display: 'flex',
};

export const commentBoxStyles: SxProps = {
  mb: 2,
  mt: 1,
  pl: 1,
  pr: 2,
  width: ['auto', 'auto', '580px'],
};

import { Container } from '@mui/material';
import Grid from '@mui/material/Grid';
import { IUserDetails } from '../../common/interfaces';
import UserCard from '../UserCard/UserCard';

const Connections = ({ details }: { details: IUserDetails }) => {
  return (
    <Container>
      <Grid
        container
        direction={{ xs: 'column', sm: 'row' }}
        justifyContent="center"
        alignItems="center"
        maxWidth="auto"
        rowSpacing={2}
      >
        {details.friends.map(({ id, username, avatar }) => (
          <Grid item key={id} xs={12} sm={6} md={4} lg={3}>
            <UserCard username={username} avatar={avatar as string} id={id} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Connections;

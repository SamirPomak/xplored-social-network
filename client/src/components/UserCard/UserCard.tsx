import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { cardStyle, mediaStyle, contentStyle } from './UserCardStyle';
import { IUserCard } from '../../common/interfaces';
import { useContext } from 'react';
import AuthContext from '../../context/AuthContext';
import ConnectButton from '../ConnectButton/ConnectButton';
import { useNavigate } from 'react-router';
import { routes } from '../../common/constants';

const UserCard = ({ username, avatar, id }: IUserCard) => {

  const { isLoggedIn, user } = useContext(AuthContext);
  const navigate = useNavigate();

  return (
    <Card variant="outlined" sx={cardStyle}>
      <CardMedia
        component="img"
        sx={mediaStyle}
        src={`http://localhost:5000/images/${avatar ?? 'default.png'}`}
        alt="random"
      />
      <CardContent sx={contentStyle}>
        {isLoggedIn ? (
          <Typography
            onClick={() => {
              window.scroll({
                top: 0,
              });
              setTimeout(() => {
                navigate(`${routes.users}/${id}`);
              }, 100);
            }}
            sx={{
              '&:hover': { cursor: 'pointer', textDecoration: 'underline' },
            }}
            gutterBottom
            variant="h5"
            component="h2"
          >
            {username}
          </Typography>
        ) : (
          <Typography gutterBottom variant="h5" component="h2">
            {username}
          </Typography>
        )}
      </CardContent>
      {isLoggedIn && user?.id !== id && (
        <CardActions>
          <ConnectButton size="large" id={id} />
        </CardActions>
      )}
    </Card>
  );
};

export default UserCard;

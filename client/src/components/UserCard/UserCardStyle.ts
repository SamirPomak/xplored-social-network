import { Theme } from '@mui/material';
import { SxProps } from '@mui/system';

export const cardStyle: SxProps<Theme> = {
  position: 'relative',
  height: '19rem',
  width: '240px',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  bgcolor: '#eeeeee',
  transition: 'transform .2s',
  '&:hover': { boxShadow: 8 },
};

export const mediaStyle: SxProps<Theme> = {
  mt: 2,
  height: '140px',
  width: '140px',
  borderRadius: '70px',
};

export const actionsStyle: SxProps<Theme> = {
  display: 'flex',
  justifyContent: 'center',
  width: '100%',
  marginBottom: 2,
};

export const contentStyle: SxProps<Theme> = {
  paddingBottom: 0,
  flexGrow: 1,
};

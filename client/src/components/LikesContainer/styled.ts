import { SxProps } from '@mui/system';

export const likesContainerUserCardStyles: SxProps = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: ['250px', '400px', '500px'],
  borderBottom: 1,
  borderColor: 'rgba(0,0,0,0.2)',
};

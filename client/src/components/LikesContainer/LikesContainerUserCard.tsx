import { Typography } from '@mui/material';
import { ILike } from '../../common/interfaces';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { Box } from '@mui/system';
import ConnectButton from '../ConnectButton/ConnectButton';
import { likesContainerUserCardStyles } from './styled';
import { useContext } from 'react';
import AppContext from '../../context/AppContext';
import { useNavigate } from 'react-router';
import { routes } from '../../common/constants';

const LikesContainerUserCard = ({ user }: { user: ILike }) => {
  const { userDetails } = useContext(AppContext);
  const navigate = useNavigate();

  const handleUsernameClick = () => {
    window.scroll({
      top: 0,
    });
    setTimeout(() => {
      navigate(`${routes.users}/${user.id}`);
    }, 100);
  };

  return (
    <Box sx={likesContainerUserCardStyles}>
      <Box sx={{ display: 'flex', flexDirection: 'row' }}>
        <Badge
          overlap="circular"
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          badgeContent={<FavoriteIcon fontSize="small" color="error" />}
        >
          <Avatar
            alt={user.username}
            src={`http://localhost:5000/images/${user.avatar ?? 'default.png'}`}
          />
        </Badge>
        <Typography
          onClick={handleUsernameClick}
          variant="h6"
          sx={{
            pl: 2,
            fontSize: ['1.1rem', '1.2rem'],
            '&:hover': { cursor: 'pointer', textDecoration: 'underline' },
          }}
        >
          {user.username}
        </Typography>
      </Box>
      {userDetails.id !== user.id && (
        <ConnectButton id={user.id} size="small" />
      )}
    </Box>
  );
};

export default LikesContainerUserCard;

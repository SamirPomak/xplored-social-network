import { ILike } from '../../common/interfaces';
import { Stack, Modal } from '@mui/material';
import { useState } from 'react';
import { Box } from '@mui/system';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { ModalStyle } from '../Users/OwnUserProfile/styled';
import LikesContainerUserCard from './LikesContainerUserCard';
import { reactionType } from '../../common/constants';

const LikesContainer = ({ users }: { users: ILike[] }) => {
  const [openLikesModal, setOpenLikesModal] = useState(false);

  return (
    <>
      <Box
        onClick={() =>
          users.filter((like) => like.reaction === reactionType.like).length >
            0 && setOpenLikesModal(true)
        }
        sx={{
          '&:hover': { cursor: 'pointer', textDecoration: 'underline' },
          mb: 1,
        }}
      >
        <FavoriteIcon fontSize="small" color="error" />{' '}
        {users.length
          ? users.filter((like) => like.reaction === reactionType.like).length
          : 0}
      </Box>
      <Modal
        open={openLikesModal}
        id="edit-modal"
        onClose={() => setOpenLikesModal(false)}
        aria-labelledby="edit-modal-title"
        aria-describedby="edit-modal-description"
      >
        <Box sx={ModalStyle}>
          <Stack spacing={2}>
            {users.map(
              (user) =>
                user.reaction === reactionType.like && (
                  <LikesContainerUserCard key={user.id} user={user} />
                )
            )}
          </Stack>
        </Box>
      </Modal>
    </>
  );
};

export default LikesContainer;

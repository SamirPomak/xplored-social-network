import Box from '@mui/material/Box';
import { LinearProgress } from '@mui/material';

const CircularIndeterminate = () => {
    return (
        <Box sx={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        }}>
            <LinearProgress />
        </Box>
    );
}

export default CircularIndeterminate;

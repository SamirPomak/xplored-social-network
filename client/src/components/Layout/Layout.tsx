import { Box } from '@mui/system';
import { ReactNode } from 'react';

const Layout = ({ children }: { children: ReactNode }) => {
  return <Box sx={{ pt: [7, 8, 8] }}>{children}</Box>;
};

export default Layout;

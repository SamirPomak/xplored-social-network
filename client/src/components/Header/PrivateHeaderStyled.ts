import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';

const ButtonStyle = { '&:hover': { bgcolor: '#3A6351' } };
const LinkStyle = { color: 'black', textDecoration: 'none' };
const ExploreIconStyles = {
  color: 'black',
  display: { xs: 'none', sm: 'block' },
  pr: 1,
  fontSize: '30px',
};

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  border: '1px solid black',
  backgroundColor: alpha(theme.palette.common.black, 0.05),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.05),
  },
  marginRight: theme.spacing(0),
  marginLeft: 0,
  width: '70%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    paddingLeft: `calc(1em + ${theme.spacing(1)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch',
    },
  },
}));

export { Search, StyledInputBase, ButtonStyle, LinkStyle, ExploreIconStyles };

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import ExploreIcon from '@mui/icons-material/Explore';
import MenuIcon from '@mui/icons-material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import { Container, Avatar } from '@mui/material';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import Logout from '@mui/icons-material/Logout';
import HomeIcon from '@mui/icons-material/Home';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useNavigate } from 'react-router-dom';
import { routes } from '../../common/constants';
import React, { useEffect, useState } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import {
  Search,
  StyledInputBase,
  ExploreIconStyles,
} from './PrivateHeaderStyled';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { userLogout } from '../../services/requests';
import AuthContext from '../../context/AuthContext';
import SnackContext from '../../context/SnackContext';
import AppContext from '../../context/AppContext';

export default function PrivateHeader() {
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    useState<null | HTMLElement>(null);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [tabValue, setTabValue] = useState<string | boolean>('home');
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const isMenuOpen = Boolean(anchorEl);
  const navigate = useNavigate();
  const theme = useTheme();
  const mediaQueryMatch = useMediaQuery(theme.breakpoints.up('xs'));
  const { user, setAuth } = React.useContext(AuthContext);
  const { setSnack } = React.useContext(SnackContext);
  const { userDetails, searchKeyword, setSearchKeyword } =
    React.useContext(AppContext);

  useEffect(() => {
    if (mediaQueryMatch) {
      setTabValue(false);
    }
  }, [mediaQueryMatch]);

  useEffect(() => {
    if (tabValue !== 'users') {
      setSearchKeyword('');
    }
  }, [tabValue]);

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleTabChange = (event: React.SyntheticEvent, newValue: string) => {
    window.scroll({
      top: 0,
    });
    setTimeout(() => {
      setTabValue(newValue);
      navigate(routes[newValue]);
    }, 100);
  };

  const updateSearch = (
    e:
      | React.MouseEvent<HTMLButtonElement, MouseEvent>
      | React.KeyboardEvent<HTMLTextAreaElement | HTMLInputElement>
  ) => {
    if (e.type === 'click') {
      if (searchKeyword.length > 0) {
        navigate('/users');
      }
    } else if ('key' in e) {
      if (e.key === 'Enter') {
        if (searchKeyword.length > 0) {
          navigate('/users');
        }
      }
    }
  };

  const handleNavigate = (path: string) => {
    window.scroll({
      top: 0,
    });
    setTimeout(() => {
      setTabValue(false);
      navigate(path);
      handleMobileMenuClose();
    }, 100);
  };

  const logout = async () => {
    setAnchorEl(null);
    handleMobileMenuClose();
    userLogout(setAuth, setSnack, navigate);
  };

  const menuId = 'primary-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem
        onClick={() => {
          window.scroll({
            top: 0,
          });
          setTimeout(() => {
            setTabValue(false);
            navigate(`${routes.users}/me`);
            handleMenuClose();
          }, 100);
        }}
      >
        My Profile
      </MenuItem>
      <MenuItem onClick={logout}>
        <ListItemIcon>
          <Logout fontSize="small" />
        </ListItemIcon>
        Logout
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = 'primary-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={() => handleNavigate(routes.home)}>
        <HomeIcon fontSize="large" sx={{ mr: 1 }} />
        Home
      </MenuItem>
      <MenuItem onClick={() => handleNavigate(routes.users)}>
        <PeopleAltIcon fontSize="large" sx={{ mr: 1 }} />
        Travelers
      </MenuItem>
      {user?.role === 2 && (
        <MenuItem onClick={() => handleNavigate(routes.admin)}>
          <SupervisorAccountIcon fontSize="large" />
          Admin
        </MenuItem>
      )}
    </Menu>
  );

  return (
    <>
      <AppBar position="fixed" sx={{ bgcolor: 'whitesmoke', color: 'black' }}>
        <Container>
          <Toolbar>
            <Box sx={{ display: 'flex', cursor: 'pointer' }}>
              <ExploreIcon sx={ExploreIconStyles} />
              <Typography
                variant="h6"
                component="div"
                onClick={() => handleNavigate(routes.home)}
                sx={{
                  display: { xs: 'none', sm: 'block' },
                  pr: 5,
                  color: 'black',
                }}
              >
                XPLORED
              </Typography>
            </Box>
            <Search>
              <StyledInputBase
                onKeyUp={updateSearch}
                value={searchKeyword}
                onChange={(e) => setSearchKeyword(e.target.value)}
                sx={{ width: '80%' }}
                placeholder="Search…"
                inputProps={{ 'aria-label': 'search' }}
              />
              <IconButton
                onClick={updateSearch}
                type="submit"
                sx={{
                  p: ['1px', '5px'],
                  color: 'black',
                  '&:hover': { bgcolor: '#1976d2' },
                }}
                aria-label="search"
              >
                <SearchIcon />
              </IconButton>
            </Search>
            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
              <Tabs value={tabValue} onChange={handleTabChange}>
                <Tab
                  icon={<HomeIcon fontSize="medium" />}
                  label="HOME"
                  value="home"
                />
                <Tab
                  icon={<PeopleAltIcon fontSize="medium" />}
                  label="TRAVELERS"
                  value="users"
                />
                {user?.role === 2 ? (
                  <Tab
                    icon={<SupervisorAccountIcon fontSize="medium" />}
                    label="ADMIN"
                    value="admin"
                  />
                ) : null}
              </Tabs>
            </Box>
            <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
              sx={{ mr: '-2px' }}
            >
              <Avatar
                alt={userDetails.username}
                src={`http://localhost:5000/images/${userDetails.avatar}`}
              />
            </IconButton>
            <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                size="large"
                color="inherit"
                aria-label="open drawer"
              >
                <MenuIcon />
              </IconButton>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </>
  );
}

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ExploreIcon from '@mui/icons-material/Explore';
import MenuIcon from '@mui/icons-material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import { Container, IconButton } from '@mui/material';
import { Link } from 'react-router-dom';
import { routes } from '../../common/constants';
import { SyntheticEvent, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';
import {
  ButtonStyle,
  LinkStyle,
  ExploreIconStyles,
  AppBarColorChange,
} from './PublicHeaderStyled';

export default function PublicHeader() {
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    useState<null | HTMLElement>(null);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const location = useLocation();
  const navigate = useNavigate();

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleLinkClick = (e: SyntheticEvent) => {
    switch ((e.target as HTMLElement).textContent) {
      case 'Home':
        navigate(routes.home);
        handleMobileMenuClose();
        break;
      case 'Travelers':
        navigate(routes.users);
        handleMobileMenuClose();
        break;
      case 'About':
        navigate(routes.about);
        handleMobileMenuClose();
        break;
      case 'Login':
        navigate(routes.signin);
        handleMobileMenuClose();
        break;
      case 'Sign up':
        navigate(routes.signup);
        handleMobileMenuClose();
        break;
    }
  };

  const mobileMenuId = 'primary-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleLinkClick}>Home</MenuItem>
      <MenuItem onClick={handleLinkClick}>Travelers</MenuItem>
      <MenuItem onClick={handleLinkClick}>About</MenuItem>
      <MenuItem onClick={handleLinkClick}>Login</MenuItem>
      <MenuItem onClick={handleLinkClick}>Sign up</MenuItem>
    </Menu>
  );

  return (
    <>
      <AppBarColorChange path={location.pathname}>
        <AppBar position="fixed" sx={{ bgcolor: '#343a40' }}>
          <Container>
            <Toolbar>
              <Box sx={{ display: 'flex', flexGrow: 1 }}>
                <ExploreIcon sx={ExploreIconStyles} />
                <Typography
                  variant="h6"
                  component="div"
                  sx={{ cursor: 'pointer' }}
                >
                  <Link to={routes.home} style={LinkStyle}>
                    XPLORED
                  </Link>
                </Typography>
              </Box>
              <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
                <Button color="inherit" sx={ButtonStyle}>
                  <Link to={routes.users} style={LinkStyle}>
                    Travelers
                  </Link>
                </Button>
                <Button color="inherit" sx={ButtonStyle}>
                  <Link to={routes.about} style={LinkStyle}>
                    About
                  </Link>
                </Button>
                <Button color="inherit" sx={ButtonStyle}>
                  <Link to={routes.signin} style={LinkStyle}>
                    Login
                  </Link>
                </Button>
                <Button color="inherit" sx={ButtonStyle}>
                  <Link to={routes.signup} style={LinkStyle}>
                    Sign up
                  </Link>
                </Button>
              </Box>
              <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
                <IconButton
                  aria-controls={mobileMenuId}
                  aria-haspopup="true"
                  onClick={handleMobileMenuOpen}
                  size="large"
                  color="inherit"
                  aria-label="open drawer"
                >
                  <MenuIcon />
                </IconButton>
              </Box>
            </Toolbar>
          </Container>
        </AppBar>
      </AppBarColorChange>
      {renderMobileMenu}
    </>
  );
}

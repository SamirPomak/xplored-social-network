import { useScrollTrigger } from '@mui/material';
import { cloneElement, ReactElement } from 'react';

const ButtonStyle = { '&:hover': { bgcolor: '#3A6351' } };
const LinkStyle = { color: 'white', textDecoration: 'none' };
const ExploreIconStyles = {
  color: 'white',
  display: { xs: 'none', sm: 'block' },
  pr: 1,
  fontSize: '30px',
};

const AppBarColorChange = (props: { children: ReactElement; path: string }) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 500,
    target: window ? window : undefined,
  });

  if (props.path !== '/home') {
    return cloneElement(props.children, { sx: { bgcolor: '#343a40' } });
  }

  return cloneElement(props.children, {
    sx: trigger
      ? { bgcolor: '#343a40', color: 'white', transition: '0.3s' }
      : {
          bgcolor: 'transparent',
          boxShadow: 0,
          color: 'white',
          transition: '0.4s',
        },
  });
};

export { ButtonStyle, LinkStyle, ExploreIconStyles, AppBarColorChange };

import { ReactNode, useContext } from 'react';
import { Navigate } from 'react-router';
import AuthContext from '../../context/AuthContext';

const RequireAuth = ({
  children,
  redirectTo,
}: {
  children: ReactNode;
  redirectTo: string;
}) => {
  const { isLoggedIn } = useContext(AuthContext);

  return isLoggedIn ? <div>{children}</div> : <Navigate to={redirectTo} />;
};

export default RequireAuth;

import { Snackbar, Alert } from '@mui/material';
import { useState } from 'react';
import { ISnack } from '../../common/interfaces';
import { ComponentChildren } from '../../common/types';
import SnackContext from '../../context/SnackContext';

const CustomSnackbar = ({ children }: ComponentChildren) => {
  const [snack, setSnack] = useState<ISnack>({
    open: false,
    message: '',
    severity: 'error',
  });

  return (
    <SnackContext.Provider value={{ setSnack }}>
      {children}
      < Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }
        }
        open={snack.open}
        autoHideDuration={4000}
        onClose={() => setSnack((prevState) => ({ ...prevState, open: false }))}
      >
        <Alert
          onClose={() => setSnack((prevState) => ({ ...prevState, open: false }))}
          severity={snack.severity}
          sx={{ width: '100%' }}
        >
          {snack.message}
        </Alert>
      </Snackbar >
    </SnackContext.Provider >
  );
};

export default CustomSnackbar;

import { Theme } from "@mui/material";
import { SxProps } from '@mui/system';

export const style: SxProps<Theme> = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    maxWidth: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export const dataGridBox: SxProps<Theme> = {
    height: 580,
    width: '85%',
    backgroundColor: 'whitesmoke',
    marginTop: 12
};

/* eslint-disable no-sparse-arrays */
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import Container from '@mui/material/Container';
import { useContext, useEffect, useMemo, useState } from 'react';
import { IconButton, Box, TextField, Typography, Chip } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import BlockIcon from '@mui/icons-material/Block';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { dataGridBox } from './adminTableStyle';
import SnackContext from '../../context/SnackContext';
import { IUserCard } from '../../common/interfaces';
import {
  adminBanUser,
  adminDeleteUser,
  getAllUsers,
} from '../../services/requests';
import { useNavigate } from 'react-router';
import { minBanReasonLength, routes } from '../../common/constants';
import CustomPopover from '../CustomPopover/CustomPopover';

const AdminTable = () => {
  const [users, setUsers] = useState<IUserCard[]>([]);
  const [currentUser, setCurrentUser] = useState<IUserCard>({
    username: '',
    avatar: '',
    id: 0,
  });
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [openBanDialog, setOpenBanDialog] = useState(false);
  const [banModalForm, setBanModalForm] = useState({
    reason: '',
    period: new Date().toISOString().slice(0, 10),
  });
  const { setSnack } = useContext(SnackContext);
  const navigate = useNavigate();

  useEffect(() => {
    getAllUsers(setUsers, setSnack);
  }, [currentUser]);

  const handleBanSubmit = (
    e:
      | React.FormEvent<HTMLFormElement>
      | React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();

    if (banModalForm.reason.length < minBanReasonLength) {
      return setSnack({
        severity: 'error',
        open: true,
        message: `Reason must be at least ${minBanReasonLength} characters!`,
      });
    }
    if (new Date(banModalForm.period).getTime() < Date.now()) {
      return setSnack({
        severity: 'error',
        open: true,
        message: `Ban date must be in the future!`,
      });
    }

    const banPeriodInDays = Math.ceil(
      (new Date(banModalForm.period).getTime() - Date.now()) /
      (1000 * 3600 * 24)
    );

    adminBanUser(
      currentUser.id,
      {
        reason: banModalForm.reason,
        period: banPeriodInDays,
      },
      setCurrentUser,
      setSnack
    );

    setBanModalForm({
      reason: '',
      period: new Date().toISOString().slice(0, 10),
    });
    setOpenBanDialog(false);
  };

  const columnsData = useMemo<GridColDef[]>(() => [
    {
      field: 'id',
      headerName: 'ID',
      width: 50,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'username',
      headerName: 'Username',
      width: 180,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <CustomPopover
            content={`View ${cellValues.row.username}'s profile`}
            hasToOpen={true}
          >
            <Typography
              onClick={() => navigate(`${routes.users}/${cellValues.row.id}`)}
              sx={{
                '&:hover': { cursor: 'pointer', textDecoration: 'underline' },
              }}
            >
              {cellValues.row.username}
            </Typography>
          </CustomPopover>
        );
      },
    },
    {
      field: 'email',
      headerName: 'Email',
      width: 250,
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'role',
      headerName: 'Role',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <>
            {cellValues.row.role === 1 ? (
              <Typography>user</Typography>
            ) : (
              <Typography>admin</Typography>
            )}
          </>
        );
      },
    },
    {
      field: 'status',
      headerName: 'Status',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        const banDate = new Date(cellValues.row.banDate as string).toLocaleString();
        return (
          <CustomPopover
            content={`Banned till: ${banDate}
                  Reason: ${cellValues.row.banReason}`}
            hasToOpen={cellValues.row.banDate !== null}
          >
            <Chip
              label={
                cellValues.row.banDate !== null ?
                  'Banned'
                  :
                  'Active'
              }
              color={
                cellValues.row.banDate !== null
                  ? 'error'
                  : 'success'
              }
            />
          </CustomPopover>
        );
      },
    },
    {
      field: 'actions',
      headerName: 'Actions',
      width: 150,
      align: 'center',
      headerAlign: 'center',
      renderCell: (cellValues) => {
        return (
          <>
            <CustomPopover
              content='Ban'
              hasToOpen={true}
            >
              <IconButton
                onClick={() => {
                  setCurrentUser(
                    users.find((u) => u.id === cellValues.row.id) as IUserCard
                  );
                  setOpenBanDialog(true);
                }}
                aria-label="ban"
                color="error"
              >
                <BlockIcon />
              </IconButton>

            </CustomPopover>
            <CustomPopover
              content='Delete'
              hasToOpen={true}
            >
              <IconButton
                onClick={() => {
                  setCurrentUser(
                    users.find((u) => u.id === cellValues.row.id) as IUserCard
                  );
                  setOpenDeleteDialog(true);
                }}
                color="error"
                aria-label="delete"
              >
                <DeleteIcon />
              </IconButton>
            </CustomPopover>
          </>
        );
      },
    },
  ], [users]);

  return (
    <Container sx={{ display: 'flex', justifyContent: 'center' }}>
      <Box sx={dataGridBox}>
        <DataGrid
          disableSelectionOnClick
          loading={users.length === 0}
          rows={users}
          columns={columnsData}
        />
      </Box>
      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Do you want to delete {currentUser?.username}?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              adminDeleteUser(
                currentUser?.id as number,
                setCurrentUser,
                setSnack
              );
              setOpenDeleteDialog(false);
            }}
          >
            Yes
          </Button>
          <Button onClick={() => setOpenDeleteDialog(false)} autoFocus>
            No
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={openBanDialog}
        onClose={() => {
          setBanModalForm({
            reason: '',
            period: new Date().toISOString().slice(0, 10),
          });
          setOpenBanDialog(false)
        }}
        aria-labelledby="dialog-title"
        aria-describedby="dialog-description"
      >
        <DialogContent>
          <Typography variant="h6">Ban {currentUser.username}</Typography>
          <hr />
          <Box component="form">
            <TextField
              id="date"
              label="Period"
              type="date"
              value={banModalForm.period}
              onChange={(e) =>
                setBanModalForm({
                  ...banModalForm,
                  period: e.target.value,
                })
              }
              sx={{ width: 220 }}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              minRows={5}
              error={banModalForm.reason.length > 0 &&
                banModalForm.reason.length < minBanReasonLength}
              helperText={
                banModalForm.reason.length < minBanReasonLength &&
                `Reason must be at least ${minBanReasonLength} characters!`
              }
              required
              fullWidth
              multiline
              margin="normal"
              id="banReason"
              label="Reason"
              name="banReason"
              autoComplete="banReason"
              value={banModalForm.reason}
              onChange={(e) =>
                setBanModalForm({
                  ...banModalForm,
                  reason: e.target.value,
                })
              }
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={(e) => handleBanSubmit(e)}
            size="small"
            color="error"
          >
            Ban
          </Button>
          <Button onClick={() => {
            setBanModalForm({
              reason: '',
              period: new Date().toISOString().slice(0, 10),
            });
            setOpenBanDialog(false)
          }}
            size="small">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </Container>
  );
};

export default AdminTable;

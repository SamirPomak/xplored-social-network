import { useContext, SyntheticEvent } from 'react';
import { Button } from '@mui/material';
import AppContext from '../../context/AppContext';
import SnackContext from '../../context/SnackContext';
import {
  acceptConnection,
  removeConnection,
  requestConnection,
} from '../../services/requests';
import { buttonStyle, buttonStyleSmall } from './styled';

const ConnectButton = ({
  id,
  size,
}: {
  id: number;
  size: 'small' | 'large';
}) => {
  const { userDetails, setTrigger } = useContext(AppContext);
  const { setSnack } = useContext(SnackContext);

  const connectButtonStatus = (id: number) => {
    const friend = userDetails?.friends.find((friend) => friend.id === id);

    if (friend) {
      if (friend.canAcceptFriendship) {
        return 'accept';
      }

      if (friend.friendshipStatus === 1) {
        return 'requested';
      }

      return 'disconnect';
    }

    return 'connect';
  };

  const handleConnectButtonClick = (e: SyntheticEvent) => {
    switch ((e.target as HTMLElement).textContent) {
      case 'connect':
        requestConnection(userDetails.id, id, setSnack, setTrigger);
        break;
      case 'disconnect':
      case 'requested':
        removeConnection(userDetails.id, id, setSnack, setTrigger);
        break;
      case 'accept':
        acceptConnection(userDetails.id, id, setSnack, setTrigger);
        break;
    }
  };

  return (
    <Button
      size="large"
      variant="outlined"
      sx={size === 'large' ? buttonStyle : buttonStyleSmall}
      onClick={handleConnectButtonClick}
    >
      {userDetails.friends ? connectButtonStatus(id) : ''}
    </Button>
  );
};

export default ConnectButton;

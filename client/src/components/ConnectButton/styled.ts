import { Theme } from '@mui/material';
import { alpha, SxProps } from '@mui/system';
export const buttonStyle: SxProps<Theme> = {
  borderRadius: '20px',
  width: '13rem',
  height: 40,
  mb: 1,
  '&:hover': (theme) => ({
    bgcolor: alpha(theme.palette.primary.main, 0.8),
    color: 'white',
  }),
};

export const buttonStyleSmall: SxProps<Theme> = {
  borderRadius: '20px',
  width: '7rem',
  height: 40,
  mb: 1,
  '&:hover': (theme) => ({
    bgcolor: alpha(theme.palette.primary.main, 0.8),
    color: 'white',
  }),
};

import { Dispatch, SetStateAction } from 'react';
import { IComment, IPost } from '../../common/interfaces';
import { postTypes } from '../../common/constants';
import CreatePost from '../CreatePost/CreatePost';
import Comment from '../Comment/Comment';

const CommentSection = ({
  comments,
  setPosts,
  postId,
}: {
  comments: IComment[];
  setPosts: Dispatch<SetStateAction<IPost[]>>;
  postId: number;
}) => {
  return (
    <>
      <CreatePost
        postType={postTypes.comment}
        setPosts={setPosts}
        postId={postId}
      />
      {comments.map((comment) => (
        <Comment
          key={comment.id}
          comment={comment}
          postId={postId}
          setPosts={setPosts}
        />
      ))}
    </>
  );
};

export default CommentSection;

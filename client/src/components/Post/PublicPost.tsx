import { Avatar, Typography, Stack } from '@mui/material';
import { Box } from '@mui/system';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { IPublicPost } from '../../common/interfaces';
import { avatarStyles, mediaContainer, postContainerStyles } from './styled';
import ReactPlayer from 'react-player';

const PublicPost = ({ post }: { post: IPublicPost }) => {
  return (
    <Box sx={postContainerStyles}>
      <Stack direction="row" spacing={2} sx={{ pt: 2, pl: 2 }}>
        <Avatar
          alt="generic avatar"
          src="http://localhost:5000/images/default.png"
          sx={avatarStyles}
        />
        <Stack>
          <Typography
            variant="h6"
            sx={{ pl: 1, fontSize: ['1.1rem', '1.2rem'] }}
          >
            private account
          </Typography>
          <Typography variant="subtitle1" sx={{ fontSize: ['0.8rem', '1rem'] }}>
            {new Date(post.createdOn).toLocaleString()}
          </Typography>
        </Stack>
      </Stack>

      {post.content && (
        <Typography sx={{ ml: 2, mt: 3 }}>{post.content}</Typography>
      )}
      {(post.embed || post.picture) && (
        <Box sx={mediaContainer}>
          {post.picture ? (
            <img
              src={`http://localhost:5000/images/${post.picture}`}
              alt={post.picture}
              style={{
                height: '400px',
                width: '100%',
                objectFit: 'cover',
              }}
            />
          ) : (
            <ReactPlayer
              width="100%"
              height="100%"
              url={post.embed as string}
              controls
            />
          )}
        </Box>
      )}
      <p>
        <FavoriteIcon sx={{ ml: 2 }} fontSize="medium" color="error" />{' '}
        {post.likesCount}
      </p>
    </Box>
  );
};

export default PublicPost;

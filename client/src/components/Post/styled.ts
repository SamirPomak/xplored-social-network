import { SxProps, styled } from '@mui/system';
export const postContainerStyles: SxProps = {
  width: ['100%', '100%', '580px'],
  height: 'auto',
  display: 'flex',
  flexDirection: 'column',
  backgroundColor: 'white',
  border: '1px solid whitesmoke',
  borderRadius: '0.8rem',
  textAlign: 'left',
};

export const avatarStyles: SxProps = {
  width: [60, 80],
  height: [60, 80],
  mr: 1,
};

export const mediaContainer: SxProps = {
  mt: 3,
  mb: 3,
  pl: 2,
  height: '400px',
  width: ['100%', '100%', '560px'],
  display: 'flex',
  justifyContent: 'center',
};

export const LikeCountContainer = styled('div')({
  paddingLeft: '10px',
  paddingRight: '10px',
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'row',
});

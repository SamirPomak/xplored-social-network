import {
  Avatar,
  Stack,
  Typography,
  IconButton,
  Button,
  ButtonGroup,
  Modal,
  TextField,
  InputLabel,
  FormControl,
  NativeSelect,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
} from '@mui/material';
import { Box } from '@mui/system';
import { IPost } from '../../common/interfaces';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import CommentIcon from '@mui/icons-material/Comment';
import {
  Dispatch,
  SetStateAction,
  SyntheticEvent,
  useContext,
  useState,
} from 'react';
import { ModalStyle } from '../Users/OwnUserProfile/styled';
import AuthContext from '../../context/AuthContext';
import SnackContext from '../../context/SnackContext';
import ReactPlayer from 'react-player';
import { deletePost, editPost, reactToPost } from '../../services/requests';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import {
  avatarStyles,
  LikeCountContainer,
  mediaContainer,
  postContainerStyles,
} from './styled';
import CommentSection from '../CommentSection/CommentSection';
import { reactionType, routes, userRoles } from '../../common/constants';
import LikesContainer from '../LikesContainer/LikesContainer';
import { useNavigate } from 'react-router';

const Post = ({
  post,
  setPosts,
}: {
  post: IPost;
  setPosts: Dispatch<SetStateAction<IPost[]>>;
}) => {
  const { user, isLoggedIn } = useContext(AuthContext);
  const { setSnack } = useContext(SnackContext);
  const [isLiked, setIsLiked] = useState(
    !!post.likes.find((like) => like.id === user?.id && like.reaction === 1)
  );
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isCommentSectionOpen, setIsCommentSectionOpen] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [modalForm, setModalForm] = useState({
    embed: post.embed || '',
    media: post.embed ? 'video' : 'picture',
    isPublic: post.isPublic ? 'true' : 'false',
  });
  const navigate = useNavigate();

  const handleLikeClick = () => {
    if (isLiked) {
      reactToPost(
        post.id,
        user?.id as number,
        reactionType.dislike,
        setPosts,
        setSnack
      );
    } else {
      reactToPost(
        post.id,
        user?.id as number,
        reactionType.like,
        setPosts,
        setSnack
      );
    }

    setIsLiked((likeStatus) => !likeStatus);
  };

  const handleDelete = () => {
    deletePost(post.id, setPosts, setSnack);
  };

  const handleMediaSelect = (event: SelectChangeEvent) => {
    setModalForm((prevState) => ({
      ...prevState,
      embed: '',
      media: event.target.value as string,
    }));
  };

  const handlePost = (e: SyntheticEvent) => {
    e.preventDefault();
    const form = new FormData(e.currentTarget as HTMLFormElement);

    editPost(post.id, form, setPosts, setSnack);

    setIsModalOpen(false);
  };

  const handleUsernameClick = () => {
    window.scroll({
      top: 0,
    });
    setTimeout(() => {
      navigate(`${routes.users}/${post.author.id}`);
    }, 100);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Box sx={postContainerStyles}>
        <Stack direction="row" spacing={2} sx={{ pt: 2, pl: 2 }}>
          <Avatar
            alt={post.author.username}
            src={`http://localhost:5000/images/${
              post.author.avatar ?? 'default.png'
            }`}
            sx={avatarStyles}
          />
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
            }}
          >
            <Stack>
              <Typography
                onClick={handleUsernameClick}
                variant="h6"
                sx={{
                  pl: 1,
                  fontSize: ['1.1rem', '1.2rem'],
                  '&:hover': { cursor: 'pointer', textDecoration: 'underline' },
                }}
              >
                {post.author.username}
              </Typography>
              <Typography
                variant="subtitle1"
                sx={{ fontSize: ['0.8rem', '1rem'] }}
              >
                {new Date(post.createdOn).toLocaleString()}
              </Typography>
            </Stack>
            {(post.author.id === user?.id ||
              user?.role === userRoles.admin) && (
              <Box sx={{ ml: [5, 15, 22] }}>
                <IconButton
                  onClick={() => setIsModalOpen(true)}
                  color="primary"
                  aria-label="edit"
                >
                  <EditIcon />
                </IconButton>
                <IconButton
                  color="error"
                  aria-label="delete"
                  onClick={() => setOpenDeleteDialog(true)}
                >
                  <DeleteIcon />
                </IconButton>
              </Box>
            )}
          </Box>
        </Stack>
        {post.content && (
          <Typography sx={{ ml: 2, mt: 3 }}>{post.content}</Typography>
        )}
        {(post.embed || post.picture) && (
          <Box sx={mediaContainer}>
            {post.picture ? (
              <img
                src={`http://localhost:5000/images/${post.picture}`}
                alt={post.picture}
                style={{
                  height: '400px',
                  width: '100%',
                  objectFit: 'cover',
                }}
              />
            ) : (
              <ReactPlayer
                width="100%"
                height="100%"
                url={post.embed as string}
                controls
              />
            )}
          </Box>
        )}
        <LikeCountContainer>
          <LikesContainer users={post.likes} />
          <Typography
            onClick={() => setIsCommentSectionOpen((status) => !status)}
            sx={{
              '&:hover': { textDecoration: 'underline', cursor: 'pointer' },
              mt: 0,
            }}
          >
            {post.comments.length} comments
          </Typography>
        </LikeCountContainer>

        {isLoggedIn && (
          <ButtonGroup sx={{ pl: 1, mb: 1 }}>
            <Button
              sx={{ color: 'black', border: '1px solid black' }}
              onClick={handleLikeClick}
            >
              {isLiked ? (
                <FavoriteIcon fontSize="large" color="error" />
              ) : (
                <FavoriteBorderIcon fontSize="large" />
              )}
            </Button>
            <Button
              onClick={() => setIsCommentSectionOpen((status) => !status)}
              sx={{ color: 'black', border: '1px solid black' }}
            >
              <CommentIcon fontSize="large" /> Comments
            </Button>
          </ButtonGroup>
        )}
        {isCommentSectionOpen && (
          <CommentSection
            comments={post.comments}
            setPosts={setPosts}
            postId={post.id}
          />
        )}
      </Box>

      <Modal
        open={isModalOpen}
        id="edit-modal"
        onClose={closeModal}
        aria-labelledby="edit-modal-title"
        aria-describedby="edit-modal-description"
      >
        <Box sx={ModalStyle}>
          <Typography variant="h6">Edit Post</Typography>
          <hr />
          <Box component="form" onSubmit={handlePost}>
            <Stack direction="row" spacing={2}>
              <Avatar
                alt={post.author.username}
                src={`http://localhost:5000/images/${
                  post.author.avatar ?? 'default.png'
                }`}
                sx={avatarStyles}
              />
              <Stack>
                <Typography variant="h6" sx={{ pl: 1 }}>
                  {post.author.username}
                </Typography>

                <FormControl sx={{ m: 1, minWidth: 80 }}>
                  <InputLabel variant="standard" htmlFor="uncontrolled-native">
                    Visibility
                  </InputLabel>
                  <NativeSelect
                    defaultValue={modalForm.isPublic}
                    inputProps={{
                      name: 'isPublic',
                      id: 'uncontrolled-native',
                    }}
                  >
                    <option value="true">Public</option>
                    <option value="false">Connections only</option>
                  </NativeSelect>
                </FormControl>
              </Stack>
            </Stack>
            <TextField
              defaultValue={post.content ?? ''}
              type="text"
              name="content"
              id="description"
              label="Description"
              minRows={4}
              fullWidth
              multiline
              margin="normal"
              sx={{ pb: 2 }}
            />
            <FormControl fullWidth sx={{ pb: 2 }}>
              <InputLabel id="media-label">Media</InputLabel>
              <Select
                labelId="media-label"
                id="media-select"
                value={modalForm.media}
                label="Media"
                onChange={handleMediaSelect}
              >
                <MenuItem value="picture">Upload a picture</MenuItem>
                <MenuItem value="video">Link a video</MenuItem>
              </Select>
            </FormControl>

            {modalForm.media === 'video' && (
              <TextField
                type="text"
                helperText="Accepts all video formats/Youtube/Vimeo/Twitch"
                name="embed"
                id="link"
                label="Video Link"
                fullWidth
                margin="normal"
                value={modalForm.embed}
                onChange={(e) =>
                  setModalForm((prevState) => ({
                    ...prevState,
                    embed: e.target.value,
                  }))
                }
                sx={{ pb: 2 }}
              />
            )}

            {modalForm.media === 'picture' && (
              <>
                <InputLabel
                  shrink
                  htmlFor="avatar"
                  sx={{ fontSize: '1.3em', fontWeight: 500 }}
                >
                  Upload picture
                </InputLabel>
                <TextField
                  fullWidth
                  name="file"
                  type="file"
                  id="avatar"
                  autoComplete="new-avatar"
                  inputProps={{ accept: 'image/*' }}
                  sx={{ pb: 2, width: ['auto', 'auto', '540px'] }}
                />
              </>
            )}
            <br />
            {modalForm.embed.length > 0 && (
              <Box
                sx={{
                  height: ['auto', 'auto', '300px'],
                  width: ['auto', 'auto', '500px'],
                }}
              >
                <ReactPlayer
                  width="100%"
                  height="100%"
                  url={modalForm.embed}
                  controls
                />
              </Box>
            )}
            <br />
            <Button type="submit" size="large" color="primary">
              Post
            </Button>
            <Button onClick={closeModal} size="large" color="error">
              Cancel
            </Button>
          </Box>
        </Box>
      </Modal>

      <Dialog
        open={openDeleteDialog}
        onClose={() => setOpenDeleteDialog(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Delete this post?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDelete}>Yes</Button>
          <Button onClick={() => setOpenDeleteDialog(false)} autoFocus>
            No
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Post;

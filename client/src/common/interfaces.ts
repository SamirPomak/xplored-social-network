import { AlertColor } from '@mui/material';

export interface IUser {
  id: number;
  username: string;
  email: string;
  role: number;
}

export interface IAuthState {
  user: IUser | null;
  isLoggedIn: boolean;
}

export interface IAuthContext {
  user: IUser | null;
  isLoggedIn: boolean;
  setAuth: React.Dispatch<React.SetStateAction<IAuthState>>;
}

export interface ISnack {
  open: boolean;
  message: string;
  severity: AlertColor | undefined;
}

export interface ISetSnack {
  setSnack: React.Dispatch<React.SetStateAction<ISnack>>;
}

export interface IUserCard {
  username: string;
  avatar: string;
  id: number;
  latitude?: number | null;
  longitude?: number | null;
}

export interface IUserDetails {
  id: number;
  username: string;
  email: string;
  role: number;
  avatar: string | null;
  banDate: string | null;
  banReason: string | null;
  lastUpdated: string;
  latitude: number | null;
  longitude: number | null;
  friends: IFriendDetails[];
}

export interface IFriendDetails {
  id: number;
  username: string;
  avatar: string | null;
  friendshipStatus?: number;
  canAcceptFriendship?: boolean;
  latitude: number | null;
  longitude: number | null;
}

export interface AppContextInterface {
  userDetails: IUserDetails;
  setUserDetails: React.Dispatch<React.SetStateAction<IUserDetails>>;
  setTrigger: React.Dispatch<React.SetStateAction<boolean>>;
  searchKeyword: string;
  setSearchKeyword: React.Dispatch<React.SetStateAction<string>>;
}

export interface IPost {
  id: number;
  content: string | null;
  picture: string | null;
  embed: string | null;
  latitude: number;
  longitude: number;
  createdOn: string;
  updatedOn: string;
  isPublic: boolean;
  author: IFriendDetails;
  likes: ILike[];
  comments: IComment[];
}

export interface ILike extends IFriendDetails {
  reaction: number;
}

export interface IComment {
  id: number;
  content: string | null;
  picture: string | null;
  embed: string | null;
  createdOn: string;
  updatedOn: string;
  author: IFriendDetails;
  likes: ILike[];
}

export interface IPublicPost {
  id: number;
  content: string | null;
  picture: string | null;
  embed: string | null;
  createdOn: string;
  likesCount: number;
}

export interface IBanUserBody {
  reason: string;
  period: number;
}

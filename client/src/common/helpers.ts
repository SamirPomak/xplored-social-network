import { credentialsLength, signInPics } from './constants';
import { ISetForm } from './types';
import validator from 'validator';
import jwt from 'jsonwebtoken';
export const getToken = (): string => localStorage.getItem('token') || '';

export const getUser = () => {
  try {
    const payload = jwt.decode(getToken()) as jwt.JwtPayload;

    return {
      id: payload.id,
      username: payload.username,
      email: payload.email,
      role: payload.role,
    };
  } catch {
    return null;
  }
};

export const validateCredentialLength = (
  value: string | number,
  field: keyof typeof credentialsLength
) => {
  if (typeof value !== 'string') return false;

  return (
    value.length === 0 ||
    (value.length >= credentialsLength[field] &&
      !value.split('').every((x) => x === ' '))
  );
};

export const validateEmail = (email: string) => {
  return validator.isEmail(email);
};

export const updateForm = (
  setForm: React.Dispatch<React.SetStateAction<ISetForm>>,
  eventTarget: HTMLInputElement | HTMLTextAreaElement
) => {
  setForm((prevState) => ({
    ...prevState,
    [eventTarget.name]: eventTarget.value,
  }));
};

export const getRandomPic = (max: number) => {
  const picIndex = Math.floor(Math.random() * max);
  return signInPics[picIndex];
};

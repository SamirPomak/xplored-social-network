export const routes: { [key: string]: string } = {
  home: '/home',
  signin: '/signin',
  signup: '/signup',
  about: '/about',
  users: '/users',
  admin: '/admin',
};

export const API = 'http://localhost:5000';

export const signUpPicUrl =
  'https://images.unsplash.com/photo-1610023926499-571d3b203226';

export const credentialsLength = {
  username: 4,
  password: 6,
};

export const minBanReasonLength = 4;

export const signInPics = [
  'https://images.unsplash.com/photo-1502301197179-65228ab57f78',
  'https://images.unsplash.com/photo-1504542982118-59308b40fe0c',
  'https://images.unsplash.com/photo-1614427771426-50fa133eebcf',
  'https://images.unsplash.com/photo-1488646953014-85cb44e25828',
  'https://images.unsplash.com/photo-1521668576204-57ae3afee860',
  'https://images.unsplash.com/photo-1524842495237-6abc737eae1f',
];

export enum postTypes {
  post = 'post',
  comment = 'comment',
}

export enum userRoles {
  user = 1,
  admin = 2,
}

export enum reactionType {
  like = 1,
  dislike = 2,
}
export const loadedUsersCount = 16;

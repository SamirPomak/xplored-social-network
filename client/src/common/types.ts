export type ISetForm = {
    [key: string]: string,
};

export type ComponentChildren = {
    children: React.ReactNode,
};

export type PropsComponent = {
    children: React.ReactNode,
    content: string,
    hasToOpen: boolean,
};

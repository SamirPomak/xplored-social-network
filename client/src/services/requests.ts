import { IComment, ILike, IPublicPost } from './../common/interfaces';
import { NavigateFunction } from 'react-router-dom';
import { API, reactionType, loadedUsersCount } from '../common/constants';
import { getUser } from '../common/helpers';
import { ISetForm } from '../common/types';
import { getToken } from '../common/helpers';
import { Dispatch, SetStateAction } from 'react';
import {
  IAuthState,
  IPost,
  ISnack,
  IUserCard,
  IUserDetails,
  IBanUserBody,
  IUser,
} from '../common/interfaces';

export const userSignUp = async (
  event: React.FormEvent<HTMLFormElement>,
  setAuth: Dispatch<SetStateAction<IAuthState>>,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  navigate: NavigateFunction
) => {
  try {
    const body = new FormData(event.currentTarget);
    const response = await fetch(`${API}/users`, {
      method: 'POST',
      body,
    });
    const data = await response.json();

    if (data.message) {
      throw new Error(data.message);
    }

    userSignIn(
      {
        username: body.get('username') as string,
        password: body.get('password') as string,
      },
      setAuth,
      setSnack,
      navigate
    );

    setSnack({
      severity: 'success',
      message: 'Welcome to Xplored!',
      open: true,
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Registration failed!',
    });
  }
};

export const userSignIn = async (
  userData: ISetForm,
  setAuth: Dispatch<SetStateAction<IAuthState>>,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  navigate: NavigateFunction
) => {
  try {
    const response = await fetch(`${API}/auth/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(userData),
    });
    const data = await response.json();
    if (data.message) {
      throw new Error(data.message);
    }

    localStorage.setItem('token', data.token);
    const user = getUser();
    setAuth({ user, isLoggedIn: true });
    navigate('/');
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'You cannot login!',
    });
  }
};

export const userLogout = (
  setAuth: Dispatch<SetStateAction<IAuthState>>,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  navigate: NavigateFunction
) => {
  fetch(`${API}/auth/logout`, {
    method: 'POST',
    headers: {
      authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then((data) => {
      if (data.statusCode === 401) {
        throw new Error(data.message);
      }

      try {
        localStorage.removeItem('token');

        setAuth({ user: null, isLoggedIn: false });
        navigate('/');
      } catch {
        throw new Error('Something went wrong!');
      }
    })
    .catch(({ message }) => {
      setSnack({
        severity: 'error',
        open: true,
        message,
      });
    });
};

export const getUserDetails = async (
  id: number | undefined,
  setUserDetails: Dispatch<SetStateAction<IUserDetails>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/users/${id}`, {
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    if (data.avatar) {
      setUserDetails(data);
    } else {
      setUserDetails({ ...data, avatar: 'default.png' });
    }
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'User data could not be loaded!',
    });
  }
};

export const updateUserDetails = async (
  form: FormData,
  setUserDetails: Dispatch<SetStateAction<IUserDetails>>,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  setAuth?: Dispatch<SetStateAction<IAuthState>>
) => {
  try {
    const response = await fetch(`${API}/users`, {
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      body: form,
    });
    const data = await response.json();

    setUserDetails(data);
    if (setAuth) {
      setAuth(
        (prevState) =>
          ({
            ...prevState,
            user: { ...prevState.user, email: data.email },
          } as IAuthState)
      );
    }

    setSnack({
      severity: 'success',
      open: true,
      message: 'Profile Updated!',
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Profile update failed!',
    });
  }
};

export const requestConnection = async (
  fromId: number,
  toId: number,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  setTrigger: Dispatch<SetStateAction<boolean>>
) => {
  try {
    const response = await fetch(`${API}/users/${fromId}/friends/${toId}`, {
      method: 'POST',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json();

    setTrigger((prevState) => !prevState);
    setSnack({
      severity: 'success',
      open: true,
      message: data.message,
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const acceptConnection = async (
  fromId: number,
  toId: number,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  setTrigger: Dispatch<SetStateAction<boolean>>
) => {
  try {
    const response = await fetch(`${API}/users/${fromId}/friends/${toId}`, {
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json();

    setTrigger((prevState) => !prevState);
    setSnack({
      severity: 'success',
      open: true,
      message: data.message,
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const removeConnection = async (
  fromId: number,
  toId: number,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  setTrigger: Dispatch<SetStateAction<boolean>>
) => {
  try {
    const response = await fetch(`${API}/users/${fromId}/friends/${toId}`, {
      method: 'DELETE',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json();

    setTrigger((prevState) => !prevState);
    setSnack({
      severity: 'success',
      open: true,
      message: data.message,
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const getUsers = async (
  setUsers: Dispatch<SetStateAction<IUserCard[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>,
  page: number,
  setPage: Dispatch<SetStateAction<number>>,
  setHasMore: Dispatch<SetStateAction<boolean>>,
  searchValue: string
) => {
  try {
    const response = await fetch(
      `${API}/users?count=${loadedUsersCount}&page=${page}&name=${searchValue}`
    );
    const data = await response.json();

    if (searchValue.length > 0 && page === 0) {
      setUsers(data);
      setPage(page + 1);
      setHasMore(true);
    } else {
      setUsers((users) => {
        const unique = data.filter(
          ({ id: id1 }: IUserCard) => !users.some(({ id: id2 }) => id2 === id1)
        );
        return [...users, ...unique];
      });
      setPage(page + 1);
    }

    if (data.length === 0 || data.length < loadedUsersCount) {
      setHasMore(false);
      setPage(0);
    }
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const getAllUsers = async (
  setUsers: Dispatch<SetStateAction<IUserCard[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/users?count=100`);
    const data = await response.json();
    setUsers(data);
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const adminDeleteUser = async (
  id: number,
  setCurrentUser: Dispatch<SetStateAction<IUserCard>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/users/${id}`, {
      method: 'DELETE',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json(); // returns deleted user

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setCurrentUser({
      username: '',
      avatar: '',
      id: 0,
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const adminBanUser = async (
  id: number,
  inputBody: IBanUserBody,
  setCurrentUser: Dispatch<SetStateAction<IUserCard>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/users/${id}/ban`, {
      method: 'POST',
      headers: {
        authorization: `Bearer ${getToken()}`,
        'Content-type': 'application/json',
      },
      body: JSON.stringify(inputBody),
    });
    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setCurrentUser({
      username: '',
      avatar: '',
      id: 0,
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const createPost = async (
  form: FormData,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/posts`, {
      method: 'POST',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      body: form,
    });
    const data = await response.json();
    setPosts((prevState) => [
      { ...data, likes: [], comments: [] },
      ...prevState,
    ]);

    setSnack({
      severity: 'success',
      open: true,
      message: 'Post created!',
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: (e as Error).message,
    });
  }
};

export const reactToPost = async (
  postId: number,
  userId: number,
  reaction: reactionType,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/posts/${postId}/react`, {
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ reaction }),
    });

    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setPosts((prevState) =>
      prevState.map((post) => {
        if (post.id === postId) {
          const like = post.likes.find((like) => like.id === userId);

          if (like) {
            like.reaction = reaction;
          } else {
            post.likes.push({ id: userId, reaction } as ILike);
          }
        }

        return post;
      })
    );
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: (e as Error).message,
    });
  }
};

export const editPost = async (
  postId: number,
  form: FormData,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/posts/${postId}`, {
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      body: form,
    });

    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setPosts((prevState) =>
      prevState.map((post) => {
        if (post.id === postId) {
          post.content = data.content;
          post.embed = data.embed;
          post.picture = data.picture;
          post.isPublic = data.isPublic;
        }

        return post;
      })
    );
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: (e as Error).message,
    });
  }
};

export const deletePost = async (
  postId: number,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/posts/${postId}`, {
      method: 'DELETE',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setPosts((prevState) => prevState.filter((post) => post.id !== postId));
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const createComment = async (
  form: FormData,
  postId: number,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/posts/${postId}/comments`, {
      method: 'POST',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      body: form,
    });
    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setPosts((prevState) =>
      prevState.map((post) => {
        if (post.id === postId) {
          post.comments.push(data);
        }

        return post;
      })
    );

    setSnack({
      severity: 'success',
      open: true,
      message: 'comment created!',
    });
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: (e as Error).message,
    });
  }
};

export const reactToComment = async (
  commentId: number,
  user: IUser | null,
  postId: number,
  reaction: reactionType,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/comments/${commentId}/react`, {
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ reaction }),
    });

    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setPosts((prevState) =>
      prevState.map((post) => {
        if (post.id === postId) {
          const comment = post.comments.find(
            (comment) => comment.id === commentId
          );
          const like = comment?.likes.find((like) => like.id === user?.id);

          if (like) {
            like.reaction = reaction;
          } else {
            comment?.likes.push({
              id: user?.id,
              reaction,
            } as ILike);
          }
        }

        return post;
      })
    );
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: (e as Error).message,
    });
  }
};

export const editComment = async (
  commentId: number,
  postId: number,
  form: FormData,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/comments/${commentId}`, {
      method: 'PUT',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
      body: form,
    });

    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setPosts((prevState) =>
      prevState.map((post) => {
        if (post.id === postId) {
          const comment = post.comments.find(
            (comment) => comment.id === commentId
          ) as IComment;
          comment.content = data.content;
          comment.picture = data.picture;
          comment.embed = data.embed;
        }

        return post;
      })
    );
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: (e as Error).message,
    });
  }
};

export const deleteComment = async (
  commentId: number,
  postId: number,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/comments/${commentId}`, {
      method: 'DELETE',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json();

    if (data.statusCode) {
      throw new Error(data.message);
    }

    setPosts((prevState) =>
      prevState.map((post) => {
        if (post.id === postId) {
          post.comments = post.comments.filter(
            (comment) => comment.id !== commentId
          );
        }

        return post;
      })
    );
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const getFeedPopular = async (
  setPosts: Dispatch<SetStateAction<IPublicPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/feed/popular`);
    const data = await response.json();

    setPosts(data);
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const getFeed = async (
  page: number,
  setPage: Dispatch<SetStateAction<number>>,
  setHasMore: Dispatch<SetStateAction<boolean>>,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(`${API}/feed?count=10&page=${page}`, {
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    });
    const data = await response.json();

    if (data.length < 10) {
      setHasMore(false);
      setPage(0);
    } else {
      setPage((prevState) => prevState + 1);
    }

    setPosts((prevState) => [...prevState, ...data]);
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

export const getUserPosts = async (
  userId: number,
  page: number,
  setPage: Dispatch<SetStateAction<number>>,
  setHasMore: Dispatch<SetStateAction<boolean>>,
  setPosts: Dispatch<SetStateAction<IPost[]>>,
  setSnack: Dispatch<SetStateAction<ISnack>>
) => {
  try {
    const response = await fetch(
      `${API}/users/${userId}/posts?count=10&page=${page}`,
      {
        headers: {
          authorization: `Bearer ${getToken()}`,
        },
      }
    );
    const data = await response.json();

    if (data.length < 10) {
      setHasMore(false);
      setPage(0);
    } else {
      setPage((prevState) => prevState + 1);
    }

    setPosts((prevState) => [...prevState, ...data]);
  } catch (e) {
    setSnack({
      severity: 'error',
      open: true,
      message: 'Oops, something went wrong!',
    });
  }
};

import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import { imageContainer, formBox } from './SignInStyle';
import { ISetForm } from '../../common/types';
import { validateCredentialLength, updateForm } from '../../common/helpers';
import { userSignIn } from '../../services/requests';
import AuthContext from '../../context/AuthContext';
import SnackContext from '../../context/SnackContext';
import { useNavigate } from 'react-router';
import Layout from '../../components/Layout/Layout';
import { useState, useContext } from 'react';
import { routes } from '../../common/constants';

const SignIn: React.FC = () => {
  const [userForm, setUserForm] = useState<ISetForm>({
    username: '',
    password: '',
  });
  const { setAuth } = useContext(AuthContext);
  const { setSnack } = useContext(SnackContext);

  const navigate = useNavigate();

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (
      !validateCredentialLength(userForm.username, 'username') ||
      !validateCredentialLength(userForm.password, 'password') ||
      Object.values(userForm).some((x) => x.length === 0)
    ) {
      return setSnack({
        severity: 'error',
        message: 'Input fields are filled incorrectly!',
        open: true,
      });
    }

    userSignIn(userForm, setAuth, setSnack, navigate);
  };

  const handleDemoLogin = () => {
    userSignIn(
      {
        username: 'admin',
        password: '123456',
      },
      setAuth,
      setSnack,
      navigate
    );
  };

  return (
    <Layout>
      <Grid container component="main" sx={{ height: '100vh' }}>
        <Grid item xs={false} sm={4} md={7} sx={imageContainer} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box sx={formBox}>
            <Avatar sx={{ m: 1, bgcolor: '#1769aa' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
            <Box
              component="form"
              noValidate
              onSubmit={handleSubmit}
              sx={{ mt: 1, width: '100%' }}
            >
              <TextField
                error={!validateCredentialLength(userForm.username, 'username')}
                helperText={
                  !validateCredentialLength(userForm.username, 'username') &&
                  'Username must be longer than or equal to 4 characters'
                }
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                onChange={(e) =>
                  updateForm(setUserForm, e.target as HTMLInputElement)
                }
              />
              <TextField
                error={!validateCredentialLength(userForm.password, 'password')}
                helperText={
                  !validateCredentialLength(userForm.password, 'password') &&
                  'Username must be longer than or equal to 4 characters'
                }
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) =>
                  updateForm(setUserForm, e.target as HTMLInputElement)
                }
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign In
              </Button>
              <Button
                onClick={handleDemoLogin}
                color="success"
                fullWidth
                variant="contained"
                sx={{ mt: 2, mb: 2 }}
              >
                Sign In with demo admin
              </Button>
              <Grid container>
                <Grid item>
                  <Link
                    href="#"
                    onClick={() => navigate(routes.signup)}
                    variant="body2"
                  >
                    {"Don't have an account? Sign Up"}
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default SignIn;

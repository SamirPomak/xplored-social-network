import { Theme } from '@mui/material';
import { SxProps } from '@mui/system';
import { signInPics } from '../../common/constants';
import { getRandomPic } from '../../common/helpers';

export const imageContainer: SxProps<Theme> = {
    backgroundImage: `url(${getRandomPic(signInPics.length)})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor: (t) =>
        t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
};

export const formBox: SxProps<Theme> = {
    my: 8,
    mx: 4,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
};

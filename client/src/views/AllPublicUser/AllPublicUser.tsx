import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import UserCard from '../../components/UserCard/UserCard';
import { getUsers } from '../../services/requests';
import { useState, useContext, useEffect } from 'react';
import SnackContext from '../../context/SnackContext';
import { IUserCard } from '../../common/interfaces';
import { IconButton } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { Search, StyledInputBase } from './AllPublicUserStyle';
import InfiniteScroll from 'react-infinite-scroll-component';
import Loader from '../../components/Loader/Loader';
import AuthContext from '../../context/AuthContext';
import AppContext from '../../context/AppContext';

const AllPublicUser = () => {
  const [users, setUsers] = useState<IUserCard[]>([]);
  const [hasMore, setHasMore] = useState(true);
  const [page, setPage] = useState(0);
  const { setSnack } = useContext(SnackContext);
  const { isLoggedIn } = useContext(AuthContext);
  const { searchKeyword, setSearchKeyword } = useContext(AppContext);

  useEffect(() => {
    getUsers(
      setUsers,
      setSnack,
      page,
      setPage,
      setHasMore,
      searchKeyword,
    );
  }, [searchKeyword]);

  return (
    <>
      <Box
        sx={{
          bgcolor: 'grey',
          pt: 8,
          pb: 6,
        }}
      >
        <Container maxWidth="sm">
          <Typography
            component="h1"
            variant="h2"
            align="center"
            color="text.primary"
            gutterBottom
          >
            Travelers
          </Typography>
          <Typography variant="h5" align="center" color="text.secondary" paragraph>
            Here you can meet travelers from all over the world.
            Share your travels and dream destinations with them.
          </Typography>
          {
            !isLoggedIn ?
              <Search>
                <StyledInputBase
                  value={searchKeyword}
                  onChange={(e) => {
                    setSearchKeyword(e.target.value)
                    setPage(0)
                    setHasMore(true);
                  }}
                  sx={{ width: '80%' }}
                  placeholder="Search…"
                  inputProps={{ 'aria-label': 'search' }}
                />
                <IconButton
                  type="submit"
                  sx={{
                    p: '5px',
                    color: 'black',
                    '&:hover': { bgcolor: '#1976d2' },
                  }}
                  aria-label="search"
                >
                  <SearchIcon />
                </IconButton>
              </Search> :
              null
          }
        </Container>
      </Box>
      <Container sx={{ py: 4 }} maxWidth="lg">
        <InfiniteScroll
          dataLength={users.length}
          next={() => getUsers(
            setUsers,
            setSnack,
            page,
            setPage,
            setHasMore,
            searchKeyword
          )}
          hasMore={hasMore}
          loader={<Loader />}
          endMessage={
            users.length > 0 ?
              <p style={{ textAlign: 'center' }}>
                <b>You have seen it all</b>
              </p> :
              null
          }
        >
          {
            users.length === 0 ?
              searchKeyword.length === 0 ?
                <Loader /> :
                <Typography>There is no match for your search!</Typography> :
              <Grid
                container
                spacing={4}
                direction={{ xs: 'column', sm: 'row' }}
                justifyContent="center"
                alignItems="center"
              >
                {users.map((user) => (
                  <Grid item key={user.id} xs={12} sm={6} md={3}>
                    <UserCard {...user} />
                  </Grid>
                ))}
              </Grid>
          }
        </InfiniteScroll>
      </Container>
    </>
  );
}

export default AllPublicUser;

import { Container, Stack, Typography, Button } from '@mui/material';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';

const AboutView = () => {
  return (
    <Container sx={{ textAlign: 'center' }}>
      <Typography variant="h2" fontWeight={900} sx={{ mt: 10 }}>
        Meet the team!
      </Typography>
      <Stack
        direction={{ xs: 'column', sm: 'row' }}
        spacing={{ xs: 1, sm: 2, md: 15 }}
        justifyContent="center"
        alignItems="center"
        marginTop={5}
        marginBottom={10}
      >
        <Card sx={{ maxWidth: 350, '&:hover': { boxShadow: 12 } }}>
          <CardMedia
            component="img"
            height="340"
            image="https://media-exp1.licdn.com/dms/image/C4E03AQGSfPzKtWci6g/profile-displayphoto-shrink_800_800/0/1634310168023?e=1642636800&v=beta&t=JGX_CatE8V0sYaToHKJoAi_NSx-xGlEStY4BNEevl68"
            alt="samir"
            sx={{ objectFit: 'fill' }}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              Samir Pomak
            </Typography>
            <Typography variant="body2" color="text.secondary">
              JavaScript enthusiast, always on the lookout for new opportunities
              to learn and grow.
            </Typography>
          </CardContent>
          <CardActions sx={{ display: 'flex', justifyContent: 'center' }}>
            <Button size="small">
              <a
                href="https://www.linkedin.com/in/samir-pomak-a93841204/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src="https://img.icons8.com/color/50/000000/linkedin.png" />
              </a>
            </Button>
            <Button size="small">
              <a
                href="https://gitlab.com/SamirPomak"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src="https://img.icons8.com/color/48/000000/gitlab.png" />
              </a>
            </Button>
          </CardActions>
        </Card>
        <Card sx={{ maxWidth: 350, '&:hover': { boxShadow: 12 } }}>
          <CardMedia
            component="img"
            height="340"
            image="https://media-exp1.licdn.com/dms/image/C5603AQGF23U3XGlesA/profile-displayphoto-shrink_800_800/0/1636646143697?e=1642636800&v=beta&t=FuWT5ywsE8reExD5S6e3u4IVjV1CjBVLcXyYJs4eNBs"
            alt="ivan"
            sx={{ objectFit: 'fill' }}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              Ivan Stamboliyski
            </Typography>
            <Typography
              variant="body2"
              color="text.secondary"
              fontSize="1.2rem"
            >
              Ex 👮‍♂️, future 👨‍💻
            </Typography>
          </CardContent>
          <CardActions sx={{ display: 'flex', justifyContent: 'center' }}>
            <Button size="small">
              <a
                href="https://www.linkedin.com/in/ivan-stamboliyski-ba72b7226/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src="https://img.icons8.com/color/50/000000/linkedin.png" />
              </a>
            </Button>
            <Button size="small">
              <a
                href="https://gitlab.com/ivan.stamboliyski"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img src="https://img.icons8.com/color/48/000000/gitlab.png" />
              </a>
            </Button>
          </CardActions>
        </Card>
      </Stack>
    </Container>
  );
};

export default AboutView;

import { Theme } from '@mui/material';
import { SxProps } from '@mui/system';
import { signUpPicUrl } from '../../common/constants';

export const imageContainer: SxProps<Theme> = {
    backgroundImage: `url(${signUpPicUrl})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor: (theme: Theme) =>
        theme.palette.mode === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
};

export const formBox: SxProps<Theme> = {
    my: 8,
    mx: 4,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
};

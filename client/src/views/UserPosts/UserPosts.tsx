import { Stack } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { IPost } from '../../common/interfaces';
import Layout from '../../components/Layout/Layout';
import Post from '../../components/Post/Post';
import SnackContext from '../../context/SnackContext';
import { getUserPosts } from '../../services/requests';
import Loader from '../../components/Loader/Loader';
import InfiniteScroll from 'react-infinite-scroll-component';

const UserPosts = ({ id }: { id: number }) => {
  const [userPosts, setUserPosts] = useState<IPost[]>([] as IPost[]);
  const { setSnack } = useContext(SnackContext);
  const [loadMore, setLoadMore] = useState(true);
  const [page, setPage] = useState(0);

  useEffect(() => {
    getUserPosts(id, page, setPage, setLoadMore, setUserPosts, setSnack);
  }, []);

  return (
    <Layout>
      <InfiniteScroll
        dataLength={userPosts.length}
        next={() =>
          getUserPosts(id, page, setPage, setLoadMore, setUserPosts, setSnack)
        }
        hasMore={loadMore}
        loader={<Loader />}
        endMessage={
          userPosts.length > 0 ? (
            <p style={{ textAlign: 'center' }}>
              <b>You have reached the end!</b>
            </p>
          ) : (
            ''
          )
        }
      >
        {userPosts.length === 0 ? (
          <Loader />
        ) : (
          <Stack justifyContent="center" alignItems="center" spacing={2}>
            {userPosts.map((post) => (
              <Post post={post} setPosts={setUserPosts} key={post.id} />
            ))}
          </Stack>
        )}
      </InfiniteScroll>
    </Layout>
  );
};

export default UserPosts;

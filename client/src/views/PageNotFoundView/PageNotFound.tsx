import { Typography } from '@mui/material';
import { Box } from '@mui/system';
import { useNavigate } from 'react-router';
import Button from '@mui/material/Button';
import { routes } from '../../common/constants';

const PageNotFound = () => {
  const navigate = useNavigate();

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        pt: 10,
      }}
    >
      <Typography
        variant="h3"
        color="error"
        sx={{ fontSize: ['1rem', '2rem', '3rem'], mb: 3 }}
      >
        Oops! Looks like you got lost, traveller!
      </Typography>
      <Button onClick={() => navigate(routes.home)} variant='contained' color='error' size='large'>Back to Home</Button>
      <Box sx={{ pt: 5, display: 'flex' }}>
        <img
          src="https://svgshare.com/i/cXP.svg"
          title="blocked-vector-image"
          style={{ width: '100%', objectFit: 'cover' }}
        />
      </Box>
    </Box>
  );
};

export default PageNotFound;

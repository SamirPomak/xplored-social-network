import { Typography } from '@mui/material';
import { Box } from '@mui/system';

const BannedView = ({ date, reason }: { date: string; reason: string }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        pt: 2,
      }}
    >
      <Typography
        variant="h3"
        color="error"
        sx={{ fontSize: ['1rem', '2rem', '3rem'] }}
      >
        You are banned until {new Date(date).toLocaleString()}
      </Typography>
      <Typography
        variant="h3"
        color="error"
        sx={{ fontSize: ['1rem', '2rem', '3rem'] }}
      >
        Reason: {reason}
      </Typography>
      <Box sx={{ pt: 5, display: 'flex' }}>
        <img
          src="https://svgshare.com/i/cVR.svg"
          title="blocked-vector-image"
          style={{ width: '100%', objectFit: 'cover' }}
        />
      </Box>
    </Box>
  );
};

export default BannedView;

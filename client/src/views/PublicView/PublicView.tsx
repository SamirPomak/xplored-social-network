import { HeroSection, HeroText } from './styled';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import { routes } from '../../common/constants';
import { Typography, Stack } from '@mui/material';
import { LinkStyle, ButtonStyle } from './styled';
import SnackContext from '../../context/SnackContext';
import { IPublicPost } from '../../common/interfaces';
import { useContext, useEffect, useState } from 'react';
import { getFeedPopular } from '../../services/requests';
import PublicPost from '../../components/Post/PublicPost';

const PublicView = () => {
  const [posts, setPosts] = useState<IPublicPost[]>([] as IPublicPost[]);
  const { setSnack } = useContext(SnackContext);

  useEffect(() => {
    getFeedPopular(setPosts, setSnack);
  }, []);

  return (
    <>
      <HeroSection>
        <HeroText>
          <Typography
            variant="h2"
            sx={{ fontSize: ['1.75rem', '3rem', '3.75rem'] }}
          >
            Explore the world
            <br />
            and share your journey!
          </Typography>
          <Button variant="contained" sx={ButtonStyle}>
            <Link to={routes.signup} style={LinkStyle}>
              Get Started
            </Link>
          </Button>
        </HeroText>
      </HeroSection>
      <Typography sx={{ mt: 5, mb: 10, textAlign: 'center' }} variant="h3">
        TRENDING POSTS 🔥
      </Typography>
      <Stack justifyContent="center" alignItems="center" spacing={2}>
        {posts.map((post) => (
          <PublicPost key={post.id} post={post} />
        ))}
      </Stack>
    </>
  );
};

export default PublicView;

import { styled } from '@mui/system';
export const HeroSection = styled('div')({
  backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(
    'https://images.unsplash.com/photo-1476514525535-07fb3b4ae5f1?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2070&q=80'
  )`,
  width: '100%',
  height: '90vh',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  position: 'relative',
});

export const HeroText = styled('div')({
  textAlign: 'center',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  color: 'white',
});

export const LinkStyle = {
  color: 'black',
  textDecoration: 'none',
};

export const ButtonStyle = {
  mt: 2,
  bgcolor: 'white',
  '&:hover': { bgcolor: '#3A6351' },
};

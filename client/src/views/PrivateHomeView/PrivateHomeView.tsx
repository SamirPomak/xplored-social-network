import { Stack } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import { IPost } from '../../common/interfaces';
import CreatePost from '../../components/CreatePost/CreatePost';
import Layout from '../../components/Layout/Layout';
import Post from '../../components/Post/Post';
import SnackContext from '../../context/SnackContext';
import { getFeed } from '../../services/requests';
import { postTypes } from '../../common/constants';
import Loader from '../../components/Loader/Loader';
import InfiniteScroll from 'react-infinite-scroll-component';

const PrivateHomeView = () => {
  const [posts, setPosts] = useState<IPost[]>([] as IPost[]);
  const { setSnack } = useContext(SnackContext);
  const [hasMore, setHasMore] = useState(true);
  const [page, setPage] = useState(0);

  useEffect(() => {
    getFeed(page, setPage, setHasMore, setPosts, setSnack);
  }, []);

  return (
    <Layout>
      <InfiniteScroll
        dataLength={posts.length}
        next={() => getFeed(page, setPage, setHasMore, setPosts, setSnack)}
        hasMore={hasMore}
        loader={<Loader />}
        endMessage={
          posts.length > 0 ? (
            <p style={{ textAlign: 'center' }}>
              <b>You have reached the end!</b>
            </p>
          ) : (
            ''
          )
        }
      >
        <Stack justifyContent="center" alignItems="center" spacing={2}>
          <CreatePost postType={postTypes.post} setPosts={setPosts} />
          {posts.length === 0 ? (
            <Loader />
          ) : (
            posts.map((post) => (
              <Post post={post} setPosts={setPosts} key={post.id} />
            ))
          )}
        </Stack>
      </InfiniteScroll>
    </Layout>
  );
};

export default PrivateHomeView;

import { createContext } from 'react';
import { AppContextInterface } from '../common/interfaces';

const AppContext = createContext({} as AppContextInterface);

export default AppContext;

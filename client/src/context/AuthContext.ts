import { createContext } from 'react';
import { IAuthContext } from '../common/interfaces';

const DEFAULT_CONTEXT: IAuthContext = {
  user: {
    id: 0,
    username: '',
    email: '',
    role: 0,
  },
  isLoggedIn: false,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setAuth: () => {},
};

const AuthContext = createContext(DEFAULT_CONTEXT);

export default AuthContext;

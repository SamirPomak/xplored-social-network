import { createContext } from 'react';
import { ISetSnack } from '../common/interfaces';

const DEFAULT_CONTEXT: ISetSnack = {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setSnack: () => { },
};

const SnackContext = createContext(DEFAULT_CONTEXT);

export default SnackContext;
